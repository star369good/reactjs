const axios = require('axios');
const fs = require('fs');

const data = { years: [] };

const getData = async () => {
  await axios.get(`http://backend-env.y3zpipbsmc.us-east-2.elasticbeanstalk.com/api/my-bike`)
  .then(res => {
    res.data.years.forEach(item => {
      data.years.push(item);
    });
  })
  .finally(() => {
    fs.writeFileSync('./years.json', JSON.stringify(data) , 'utf-8');
  });
}

getData();
