const categories = [
  {
    id: 1,
    name: "Air & Fuel"
  },
  {
    id: 2,
    name: "Audio & Speakers"
  },
  {
    id: 3,
    name: "Batteries & Electrical"
  },
  {
    id: 4,
    name: "Bike Protection"
  },
  {
    id: 5,
    name: "Body Fairing & Fender"
  },
  {
    id: 6,
    name: "Brakes"
  },
  {
    id: 7,
    name: "Dash & Gauges"
  },
  {
    id: 8,
    name: "Drive & Transmission"
  },
  {
    id: 9,
    name: "Engine"
  },
  {
    id: 10,
    name: "Exhaust"
  },
  {
    id: 11,
    name: "Filters"
  },
  {
    id: 12,
    name: "Foot Controls"
  },
  {
    id: 13,
    name: "Gas & Oil Tanks"
  },
  {
    id: 14,
    name: "Handlebars & Controls"
  },
  {
    id: 15,
    name: "Lighting"
  },
  {
    id: 16,
    name: "Mirrors"
  },
  {
    id: 17,
    name: "Seats & Sissy Bars"
  },
  {
    id: 18,
    name: "Suspension & Frame"
  },
  {
    id: 19,
    name: "Wheel & Axle"
  },
  {
    id: 20,
    name: "Windshields & Windscreens"
  }
];

export const categoriesMap = {
  1: {
    name: "Air & Fuel",
    selection: null
  },
  2: {
    name: "Audio & Speakers",
    selection: null
  },
  3: {
    name: "Batteries & Electrical",
    selection: null
  },
  4: {
    name: "Bike Protection",
    selection: null
  },
  5: {
    name: "Body Fairing & Fender",
    selection: null
  },
  6: {
    name: "Brakes",
    selection: null
  },
  7: {
    name: "Dash & Gauges",
    selection: null
  },
  8: {
    name: "Drive & Transmission",
    selection: null
  },
  9: {
    name: "Engine",
    selection: null
  },
  10: {
    name: "Exhaust",
    selection: null
  },
  11: {
    name: "Filters",
    selection: null
  },
  12: {
    name: "Foot Controls",
    selection: null
  },
  13: {
    name: "Gas & Oil Tanks",
    selection: null
  },
  14: {
    name: "Handlebars & Controls",
    selection: null
  },
  15: {
    name: "Lighting",
    selection: null
  },
  16: {
    name: "Mirrors",
    selection: null
  },
  17: {
    name: "Seats & Sissy Bars",
    selection: null
  },
  18: {
    name: "Suspension & Frame",
    selection: null
  },
  19: {
    name: "Wheel & Axle",
    selection: null
  },
  20: {
    name: "Windshields & Windscreens",
    selection: null
  }
}

export default categories;
