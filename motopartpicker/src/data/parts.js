import airintake from './air-intake-data.json';
import exhaust from './exhaust-data.json';
import ftire from './ftire-data.json';
import grips from './grips-data.json';
import handlebar from './handlebar-data.json';
import rtire from './rtire-data.json';
import suspension from './suspension-data.json';

export { airintake, exhaust, ftire, grips, handlebar, rtire, suspension };
