// Includes the font awesome brand buttons for social media
export const brandButtons = [
	{
		link: "#instagram",
		name: "instagram",
		title: "Instagram"
	},
	{
		link: "#reddit",
		name: "reddit-alien",
		title: "Reddit"
	},
	{
		link: "#twitter",
		name: "twitter",
		title: "Twitter"
	}
];
