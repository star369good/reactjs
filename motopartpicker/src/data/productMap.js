const productMap = {
  'handlebar': 'Handlebar',
  'exhaust': 'Exhaust',
  'grips': 'Grips',
  'airintake': 'Air Intake',
  'ftire': 'Front Tire',
  'rtire': 'Rear Tire',
  'suspension': 'Suspension'
};

export default productMap
