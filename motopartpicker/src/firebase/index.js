import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import React from 'react';

const config = {
  apiKey: "AIzaSyDllRCX26-dzZS2onFDZfaUn7x13EVsDAc",
  authDomain: "motopartpicker-1849e.firebaseapp.com",
  databaseURL: "https://motopartpicker-1849e.firebaseio.com",
  projectId: "motopartpicker-1849e",
  storageBucket: "motopartpicker-1849e.appspot.com",
  messagingSenderId: "546277294446"
};


export default class Firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();
    this.db = app.firestore();
    this.provider = null;

    this.doLoginWithGoogle = this.doLoginWithGoogle.bind(this);
  }
  doLoginWithGoogle() {
    this.provider = new app.auth.GoogleAuthProvider();
    this.auth.signInWithRedirect(this.provider)
      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
      });
  }
  doCreateUserWithEmailAndPassword(email, password) {
    this.auth.createUserWithEmailAndPassword(email, password)
      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
      });
  }
  doSignInWithEmailAndPassword(email, password) {
    this.auth.signInWithEmailAndPassword(email, password)
      .catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
      });
  }
  doSignOut = () => this.auth.signOut();

  // Database Logic
  retrieveUser = async (user) => {
    const userDb = this.db.collection('users').doc(user.uid);
    let result;
    await userDb.get().then(function(doc) {
      if (doc.exists) {
        console.log("User Retrieved!");
        result = doc.data();
      } else {
        console.log("User Not Found!");
        result = null;
      }
    }).catch(function(error) {
      console.log("Error getting document:", error);
      result = null;
    });
    return result;
  };

  saveNewBuildList = async (user, data) => {
    const userDb = this.db.collection('users');
    await userDb.doc(user.uid).collection('lists').doc(data.name).set({
      name: data.name,
      list: data.list,
      description: data.description
    })
    .then(() => {
      console.log("Document successfully written!");
    })
    .catch((error) => {
      console.error("Error writing document: ", error);
    });
  };

  loadUserLists = async (user) => {
    const docRef = this.db.collection('users').doc(user.uid).collection('lists');
    const listArray = [];
    await docRef.get().then((q) => {
        q.forEach((list) => listArray.push(list.data()));
    });
    return listArray;
  };  
}

const FirebaseContext = React.createContext(null);
const withFirebase = Component => props => (
  <FirebaseContext.Consumer>
    {firebase => <Component {...props} firebase={firebase} />}
  </FirebaseContext.Consumer>
);
export { FirebaseContext, withFirebase };