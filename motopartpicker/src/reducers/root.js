const initialState = {
  isLoggedIn: false,
  isAttemptingToLogin: false, 
  user: null
};

const root = (state = initialState, action) => {
  switch (action.type) {
    case 'IS_ATTEMPTING_TO_LOGIN':
      return {
        ...state,
        isAttemptingToLogin: true
      }
    case 'LOGIN_USER':
      return {
        ...state,
        isLoggedIn: true,
        isAttemptingToLogin: false,
        user: action.user
      }
    case 'SIGN_OUT_USER': 
      return {
        ...state,
        isLoggedIn: false,
        isAttemptingToLogin: false,
        user: null
      }
    default:
      return state;
  }
}

export default root;
