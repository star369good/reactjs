import { categoriesMap } from '../data/categories';

const initialState = {
  bikeType: 1,
  selectedParts: {
    ...categoriesMap
  },
  selectedListName: null
};

const buildCreatorData = (state = initialState, action) => {
  switch (action.type) {
    case 'PART_SELECTION':
      let part  = action.part;
      part.product = part.title;
      part.productLink = part.prices[0].link;
      part.price = part.prices[0].price;
      part.vendor = part.brand;
      let nm = state.selectedParts[`${action.selection}`].name;
      let sel = state.selectedParts[`${action.selection}`].selection;
      if(!sel) sel = [];
      sel.push(part);
      return {
        ...state,
        selectedParts: {
          ...state.selectedParts
          , [`${action.selection}`]: {name: nm, selection: sel}
        }
      }
    case 'CANCEL_PART_SELECTION':
      return {
        ...state,
        selectedParts: {
          ...state.selectedParts,
          [`${action.part}`]: null
        }
      }
    case 'SELECT_BIKE_TYPE': 
      return {
        ...state,
        bikeType: action.bikeType
      }
    case 'LOAD_BUILD':
      return {
        ...state,
        selectedParts: {...action.build.list},
        selectedListName: action.build.name
      }
    default:
      return state;
  }
}

export default buildCreatorData;
