import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import ApiStore from './ApiStore';
import buildCreatorData from './buildCreatorData';
import root from './root';

export default (history) => combineReducers({
  router: connectRouter(history),
  apiStore: ApiStore,
  root,
  buildCreatorData
});
