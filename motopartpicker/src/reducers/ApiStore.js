import { createAction, createReducer } from 'redux-act';

const initialState = {
  bikeTypeId: null,
  yearId: null,
  makeId: null,
  modelId: null
}

// Create an action creator (description is optional)
export const addSearchQueries = createAction('API/ADD_SEARCH_QUERIES');
export const updateSearchQuery = createAction('API/UPDATE_SEARCH_QUERY');

// Create a reducer
const ApiStore = createReducer({}, initialState);

ApiStore.on(addSearchQueries, (state, queries = {}) => {
  return {
    ...state,
    ...queries
  }
});
ApiStore.on(updateSearchQuery, (state, query, value) => {
  return {
    ...state, 
    [query]: value 
  }
});

export default ApiStore;