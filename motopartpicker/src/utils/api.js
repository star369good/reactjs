import axios from 'axios';

const URL = 'http://134.209.67.225/api/my-bike';

export const getPartsForCategory = async (bikeTypeId, yearId, makeId, modelId, catId) => {
	console.log(
		URL 
		+ '?type=' + bikeTypeId 
		+ '&year=' + yearId 
		+ '&make=' + makeId 
		+ '&model=' + modelId
		+ '&cat=' + catId		
	);
	return await axios.get(
		URL 
		+ '?type=' + bikeTypeId 
		+ '&year=' + yearId 
		+ '&make=' + makeId 
		+ '&model=' + modelId
		+ '&cat=' + catId
	)
	.then(res => {
		return res.data.parts.data;
	});
}
