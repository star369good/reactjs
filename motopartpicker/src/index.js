import React from 'react';
import { hydrate, render } from "react-dom";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware, ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import App from './components/App';
import Firebase, { FirebaseContext } from './firebase';
import "bootstrap/dist/css/bootstrap.min.css";
import './index.css';

const history = createBrowserHistory();
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer(history),
  composeEnhancer(
    applyMiddleware(
      thunk,
      routerMiddleware(history)
    )
  )
);
const rootElement = document.getElementById("root");
const Site = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <FirebaseContext.Provider value={new Firebase()}>
        <App />
      </FirebaseContext.Provider>
    </ConnectedRouter>
  </Provider>
);
if (rootElement.hasChildNodes()) {
  hydrate(Site, rootElement);
} else {
  render(Site, rootElement);
}

// Causes issues with static site generation due to caching.
// Commenting out for now.
// registerServiceWorker();
