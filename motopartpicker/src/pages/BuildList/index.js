import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withFirebase } from '../../firebase';
import { loadSavedBuildAndShowBuild } from '../../actions';
import { Link } from 'react-router-dom';
import './BuildList.css';
import productMap from '../../data/productMap';

class BuildList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savedLists: [],
      nameArr: [],
      selected: null,
      currentItem: null
    };
    this.getSavedLists = this.getSavedLists.bind(this);
    this.onHandleChange = this.onHandleChange.bind(this);
    this.onDeleteList = this.onDeleteList.bind(this);
  }

  componentDidMount() {
    const { root } = this.props;
    if (root.isLoggedIn) {
      this.loadUserLists();
    }
  }

  async loadUserLists() {
    const { firebase, root } = this.props;
    const savedLists = await firebase.loadUserLists(root.user)
      .catch(err => console.error(err));
    if (savedLists.length > 0) {
      const nameArr = savedLists.map(i => i.name);
      const selected = 0;
      this.setState({ savedLists, nameArr, selected });
    }
  }

  getSavedLists() {
    const { savedLists, nameArr } = this.state;
    if (savedLists === null) {
      return <option>You have no saved builds</option>;
    } else {
      return nameArr.map(name =>
        <option key={name}>{name}</option>
      );
    }
  }

  loadSavedBuild() {
    const { selected, savedLists } = this.state;
    const selectedList = savedLists[selected];
    console.log(savedLists[selected]);
    this.props.loadSavedBuildAndShowBuild(selectedList);
  }

  showSavedBuild() {
    const { selected, savedLists } = this.state;
    const itemArr = [
      'handlebar',
      'exhaust',
      'grips',
      'airintake',
      'ftire',
      'rtire',
      'suspension'
    ];
    return itemArr.map((item, idx) => {
      const selectedList = savedLists[selected];
      if (selectedList.list && selectedList.list[item]) {
        const cItem = selectedList.list[item];
        return (
          <div key={idx} className='col-md-12'>
            <div className='card mb-2 shadow-sm'>
              <img className='card-img' src={cItem.image} />
              <div className='card-body-container'>
                 {productMap[item]}
                 <a href={cItem.productLink} target='_blank'><h5 className='card-title'>{cItem.manufacturer} {cItem.product}</h5></a>
                 <p>Find it on <a href={cItem.productLink} className='link' target='_blank'>{cItem.vendor}</a> for {cItem.price}</p>
                 <a href={cItem.productLink} target='_blank' className='btn btn-outline-primary btn-sm'> Buy It </a>
              </div>
            </div>
          </div>
        );
      }
    });
  }

  getIconRow() {
    if (this.state.selected !== null) {
      return (
        <div className='icon-row'>
          <button className='btn btn-sm btn-outline-dark mr-4'>Share</button>
          <button className='btn btn-sm btn-outline-primary mr-4' onClick={() => this.loadSavedBuild()}>Edit</button>
          <button className='btn btn-sm btn-outline-danger' onClick={this.onDeleteList}>Delete</button>
        </div>
      );
    }
  }

  deleteSelectedListItem() {
    const { selected } = this.state;
    // fill in //
  }

  onHandleChange(evt) {
    this.setState({
      currentItem: this.state.savedLists[evt.target.value],
      selected: evt.target.value
    })
  }

  onDeleteList() {
    // prompt with 'Are you sure' modal
    // delete the entry from localstore if yes.
    if (window.confirm("Are you sure you want to delete this list?")) {
      this.deleteSelectedListItem();
    }
  }

  render() {
    const { selected } = this.state;
    return (
      <div className='section-container build-lists-container'>
        <div className='container-fluid p-0'>
          <div className='shadow-sm p-2'>
            <select className="form-control" onChange={this.onHandleChange}>
              {this.getSavedLists()}
            </select>
          </div>
          <div className='col-md-12 pl-3 pr-3 p-0 mt-4 parts-table-header mb-3'>
            <h5 className='lead-text'>My Saved Parts Lists </h5>
            {this.getIconRow()}
          </div>
        </div>
        <div className='container-fluid p-0'>
          {(selected !== null) ?
            this.showSavedBuild() :
            <div className='no-saved-list-header'> You have no saved lists! Try creating a motorcycle <Link className='link' to='/build'>parts list</Link>!</div>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    root: state.root
  }
}

const dispatchToProps = {
  loadSavedBuildAndShowBuild
}

export default connect(mapStateToProps, dispatchToProps)(withFirebase(BuildList));
