import React from 'react';

const PrivacyPolicy = (props) => {
  return (
    <div>
      <div className='partner-list-jumbo'>
        <div className='container mt-0 mb-5'>
          <div className='row'>
            <div className='col-md-10 offset-md-1 mt-5'>
              <h5 className='lead-text'>Privacy Notice</h5>
              <p>This privacy notice discloses the privacy practices for Moto Part Picker (http://www.MotoPartPicker.com). This privacy notice applies solely to information collected by this website. It will notify you of the following:</p>
              <ul className="nav flex-column pl-5">
                <li className="nav-item">
                  1. What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.
                </li>
                <li className="nav-item">
                  2. What choices are available to you regarding the use of your data.
                </li>
                <li className="nav-item">
                  3. The security procedures in place to protect the misuse of your information.
                </li>
                <li className="nav-item">
                  4. How you can correct any inaccuracies in the information.
                </li>
              </ul>
              <div className='mt-4 bold'>Information Collection, Use, and Sharing</div>
              <p>We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone. We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order. Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.
              </p>
              <div className='mt-4 bold'>Your Access to and Control Over Information</div>
              <p>You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p>
              <ul className="nav flex-column pl-5">
                <li className="nav-item">
                  • See what data we have about you, if any.
                </li>
                <li className="nav-item">
                  • Change/correct any data we have about you.
                </li>
                <li className="nav-item">
                  • Have us delete any data we have about you.
                </li>
                <li className="nav-item">
                  • Express any concern you have about our use of your data.
                </li>
              </ul>
              <div className='mt-4 bold'>Security</div>
              <p>We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline. Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page. While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.
              </p>
              <p className='bold'>If you feel that we are not abiding by this privacy policy, you should contact us immediately via email at info@motopartpicker.com.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PrivacyPolicy;
