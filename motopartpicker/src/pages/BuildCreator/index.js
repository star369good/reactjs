import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactGA from 'react-ga';
import './BuildCreator.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import PartRow from '../../components/PartRow';
import SelectionView from "../../components/SelectionView";
import { onPartSelection, onCancelPartSelection } from '../../actions';
import * as currency from 'currency.js';
import categories from '../../data/categories';
import BikePicker from '../../components/BikePicker';
import { withFirebase } from '../../firebase';
import * as API from '../../utils/api';
library.add(fab);

class BuildCreator extends Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.categories = categories;
    for(var i = 0; i < this.categories.length; i++){
      this.categories[i].isShow = true;
    }
    this.state = {
      hasSavedList: false
    };
    this.onEnterPartSelectionView = this.onEnterPartSelectionView.bind(this);
  }

  calculateTotal() {
    const { selectedParts } = this.props.buildCreatorData;
    let total = 0;
    for (let part in selectedParts) {
      const item = selectedParts[part].selection;
      if (item) {
        for(let i in item){
          total = currency(total).add(currency(item[i].price));
        }
      }
    }
    return currency(total).format();
  }

  getShoppingTotal() {
    const total = this.calculateTotal();
    if (currency(total) > 0) {
      return <h5>Total: ${total}</h5>;
    }
  }

  getHiddenCat(){
    const { apiStore } = this.props;
    const { bikeTypeId, yearId, makeId, modelId } = apiStore;
    const hasSearched = bikeTypeId && yearId && makeId && modelId;
    // console.log('get hidden categories.' + hasSearched);
    if(!hasSearched) return ;
    return (
          <form className="form">
            <fieldset>
              <legend>Categories we couldn't find</legend>
              { this.categories.map(cat => {
                return (!cat.isShow)? <p className="pl-3" key={cat.id}>{cat.name}</p>: ''
              }) }
            </fieldset>
          </form>
          );
  }

  async onSaveToDatabase() {
    const { firebase, root } = this.props;
    const input = this.refs.buildSaveName;
    if (input.value) {
      const { selectedParts } = this.props.buildCreatorData;
      const name = input.value;
      const description = '';
      const data = {
        name,
        list: selectedParts,
        description
      }
      await firebase.saveNewBuildList(root.user, data)
      .then(() => {
        this.setState({hasSavedList: true});
      })
      .catch((err) => {
        console.error(err);
      });
    }
  }

  onShowSave() {
    const { root, buildCreatorData } = this.props;
    const { hasSavedList } = this.state;
    const { selectedListName = '' } = buildCreatorData;
    const total = this.calculateTotal();
    if (currency(total) > 0) {
      if (root.isLoggedIn) {
        return (
          <div className='form-inline form-group'>
            {hasSavedList ? <small className='mr-2 text-success'>Saved!</small> : null}
            <input defaultValue={selectedListName} ref='buildSaveName' type="text" className="form-control form-control-md custom-input" placeholder='Name Your Build' />
            <button className='btn btn-dark ml-2' onClick={() => this.onSaveToDatabase()}>
              Save
            </button>
          </div>
        );
      }
      return (
        <div className='sign-up-text'>Sign Up or Log In to save your list!</div>
      );
    }
  }

  onSelect(part) {
    const { selecting } = this.state;
    this.props.onPartSelection(part, selecting);
    const { selectedParts } = this.props.buildCreatorData;
    console.log(selectedParts);
    this.setState({selecting: false, hasSavedList: false});
  }

  onEnterPartSelectionView(part) {
    ReactGA.event({
      category: 'Build',
      action: 'Selecting a part',
      label: 'Selecting a part'
    });
    this.setState({selecting: part});
  }

  getPartRows() {
    const { selectedParts } = this.props.buildCreatorData;
    return this.categories.map(cat => {
      if(!cat.isShow) return ;
      return (
        <PartRow
          key={cat.id}
          selection={selectedParts[cat.id].selection}
          onPartSelectionView={(part) => this.onEnterPartSelectionView(part)}
          onCancel={() => this.props.onCancelPartSelection(null)}
          partType={cat.id}
          display={cat.name}
          isShow={cat.isShow}
        />
      );
    });
  }

  async getPartsFromApi() {
    const { apiStore } = this.props;
    const { bikeTypeId, yearId, makeId, modelId } = apiStore;
    const hasSearched = bikeTypeId && yearId && makeId && modelId;
    if(hasSearched){
      for(var i = 0; i < this.categories.length; i++){
        try {
          if(this._isMounted){
            const parts = await API.getPartsForCategory(bikeTypeId, yearId, makeId, modelId, i+1);
            this.categories[i].isShow = (parts.length > 0);
            this.setState({categories: this.categories});
          }
        } catch(err) {
          console.log(err)
        }
      }
    }    
  }

  componentDidMount() {
    this._isMounted = true;
    if(this._isMounted) this.getPartsFromApi();
  }

  setState(params){
    if(this._isMounted) super.setState(params);
  }

  componentWillUnmount(){
    this._isMounted = false;
  }

  renderPreSearchView() {
    return (
      <div className='pre-search-view-container'>
        <div className='pre-search-view-text-container'>
          <div className='pre-search-text'>
            <p>Search for your year, make and model.</p>
          </div>
        </div>
        {/* {console.log('presearching...')} */}
        {this.renderPartRows(7)}
      </div>
    );
  }

  renderLoadingView() {
    return (
      <div className='pre-search-view-container'>
        <div className='pre-search-view-text-container'>
          <div className='pre-search-text'>
            <p>Retrieving Parts...</p>
          </div>
        </div>
        {/* {console.log('loading...')} */}
        {this.renderPartRows(7)}
      </div>
    );
  };

  renderPartRows(count) {
    return (
      <table className="table table-striped mt-2">
        <thead>
          <tr>
            <th>Part</th>
            <th>Selection</th>
            <th className='mobileTableCell'>Base</th>
            <th className='mobileTableCell'>Promo</th>
            <th className='mobileTableCell'>Shipping</th>
            <th className='mobileTableCell'>Tax</th>
            <th className='mobileTableCell'>Price</th>
            <th className='mobileTableCell'>Where</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>{count ? this.getPartRows().slice(0, count) : this.getPartRows().slice(0, count)}</tbody>
      </table>
    );
  }

  render() {
    const { selecting } = this.state;
    const { apiStore } = this.props;
    const { bikeTypeId, yearId, makeId, modelId } = apiStore;
    const { selectedParts } = this.props.buildCreatorData
    const hasSearched = bikeTypeId && yearId && makeId && modelId;
    if (selecting) {
      return (
        <SelectionView
          apiStore={this.props.apiStore}
          selecting={selecting}
          onCancel={(isShow) => (this.setState({selecting: false}), this.categories[selecting-1].isShow = isShow)}
          onSelect={(part) => this.onSelect(part)}
          selectedParts  = { selectedParts }
        />
      );
    }

    return (
      <div className='build-creator-container section-container'>
        <div className='shadow-sm p-2 text-center' hidden={hasSearched}>
          <p className='mt-3 mb-2'><FontAwesomeIcon icon='motorcycle' size='lg' /> <span className='ml-2'>Search by Motorcycle</span></p>
          <BikePicker {...this.props} />
        </div>
        <div className='container-fluid p-0'>
          <div className='row m-0'>
            <div className='col-md-12 pl-3 pr-3 p-0'>
              <div className='mt-4 parts-table-header'>
                <h5 className='lead-text mt-2'> Create a Parts List </h5>
                {this.onShowSave()}
              </div>
              <hr />
              {!hasSearched ? this.renderPreSearchView() : this.renderPartRows()}
            </div>
          </div>
          <div className="mt-3 pl-3">{this.getShoppingTotal()}</div>
          <div className="row p-40 m-3">            
            {this.getHiddenCat()}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    buildCreatorData: state.buildCreatorData,
    root: state.root,
    apiStore: state.apiStore
  }
}

const dispatchToProps = {
  onPartSelection,
  onCancelPartSelection
}

export default connect(mapStateToProps, dispatchToProps)(withFirebase(BuildCreator));
