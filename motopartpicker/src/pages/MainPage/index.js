import React from 'react';
import './MainPage.css';
import BikePicker from '../../components/BikePicker';

const MainPage = (props) => {
  this.props = props;
  return (
    <div>
      <div className='jumbotron-new'>
        <div className='container mt-0'>
          <div className='row'>
            <div className='col-md-12 mt-3 mb-5 text-center'>
              <h5 className='lead-text'>Start Here</h5>
              <p>Select the year, make and model of your motorcycle.</p>
              <BikePicker {...this.props} />
              </div>
            </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
