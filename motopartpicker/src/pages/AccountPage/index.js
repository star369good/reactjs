import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { faReddit } from '@fortawesome/free-brands-svg-icons';
library.add(faInstagram, faFacebook, faReddit);

const AccountPage = (props) => {
  return (
    <div className='account-page-container container'>
      <div className='row'>
        <div className='col-md-8 offset-md-2 mt-5 mb-3'>
          <h5 className='lead-text'>Welcome First Name!</h5>
          <ul className="nav mt-4">
            <li className="nav-item">
              <a className="nav-link selected"><i className='fa fa-list'></i> My Saved Lists</a>
            </li>
            <li className="nav-item">
              <a className="nav-link"><i className='fa fa-user'></i> Account Details</a>
            </li>
            <li className="nav-item">
              <a className="nav-link"><i className='fa fa-gear'></i> Settings</a>
            </li>
          </ul>
          <div className="list-group mt-3 mb-4">
            <a className="list-group-item list-group-item-action">
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">Saved List 1</h5>
                <small>3 days ago</small>
              </div>
              <p className="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
              <div>Share to <span className='ml-2'><FontAwesomeIcon className='mr-2' icon={["fab", "instagram"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "facebook"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "reddit"]} /></span></div>
            </a>
            <a className="list-group-item list-group-item-action">
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">Saved List 2</h5>
                <small className="text-muted">3 days ago</small>
              </div>
              <p className="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
              <div>Share to <span className='ml-2'><FontAwesomeIcon className='mr-2' icon={["fab", "instagram"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "facebook"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "reddit"]} /></span></div>
            </a>
            <a className="list-group-item list-group-item-action">
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">Saved List 3</h5>
                <small className="text-muted">3 days ago</small>
              </div>
              <p className="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
              <div>Share to <span className='ml-2'><FontAwesomeIcon className='mr-2' icon={["fab", "instagram"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "facebook"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "reddit"]} /></span></div>
            </a>
            <a className="list-group-item list-group-item-action">
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">Saved List 4</h5>
                <small className="text-muted">3 days ago</small>
              </div>
              <p className="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
              <div>Share to <span className='ml-2'><FontAwesomeIcon className='mr-2' icon={["fab", "instagram"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "facebook"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "reddit"]} /></span></div>
            </a>
            <a className="list-group-item list-group-item-action">
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">Saved List 5</h5>
                <small className="text-muted">3 days ago</small>
              </div>
              <p className="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
              <div>Share to <span className='ml-2'><FontAwesomeIcon className='mr-2' icon={["fab", "instagram"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "facebook"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "reddit"]} /></span></div>
            </a>
            <a className="list-group-item list-group-item-action">
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">Saved List 6</h5>
                <small className="text-muted">3 days ago</small>
              </div>
              <p className="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
              <div>Share to <span className='ml-2'><FontAwesomeIcon className='mr-2' icon={["fab", "instagram"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "facebook"]} /> <FontAwesomeIcon className='mr-2' icon={["fab", "reddit"]} /></span></div>
            </a>
          </div>
          <div className="btn-group float-right" role="group" aria-label="First group">
            <button type="button" className="btn btn-light btn-sm">1</button>
            <button type="button" className="btn btn-light btn-sm">2</button>
            <button type="button" className="btn btn-light btn-sm">3</button>
            <button type="button" className="btn btn-light btn-sm"><FontAwesomeIcon icon={["fas", "arrow-alt-right"]} /></button>
          </div>
        </div>
      </div>
    </div>

  );
}

export default AccountPage;
