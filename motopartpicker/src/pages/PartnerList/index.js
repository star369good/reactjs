import React from 'react';

const PartnerList = (props) => {
  return (
    <div className='container mt-0 mb-5'>
      <div className='row'>
        <div className='col-md-12 mt-5'>
          <h5 className='lead-text'>Partner List</h5>
          <p>Our parts come from the following suppliers:</p>
          <ul className="nav flex-column">
            <li className="nav-item">
              <a className="nav-link pl-0">RevZilla</a>
            </li>
            <li className="nav-item">
              <a className="nav-link pl-0">J&P Cycles</a>
            </li>
            <li className="nav-item">
              <a className="nav-link pl-0">MotoSport</a>
            </li>
            <li className="nav-item">
              <a className="nav-link pl-0">Chapparal Motorsports</a>
            </li>
            <li className="nav-item">
              <a className="nav-link pl-0">FortNine</a>
            </li>
            <li className="nav-item">
              <a className="nav-link pl-0">Rocky Mountain ATV / MC</a>
            </li>
          </ul>
          <p className='mt-4'><a className='text-primary'>Click Here</a> to join our growing supplier list.</p>
        </div>
      </div>
    </div>
  );
}

export default PartnerList;
