import React from 'react';

const Contact = (props) => {
  return (
    <div className='container'>
      <div className='row mt-3 mb-5'>
        <div className='col-md-6 offset-md-3'>
          <form className="form-signin mt-5">
          <h1 className="h3 mb-2 font-weight-normal lead-text">Contact Us</h1>
          <p className='mb-4'>Send us feedback, questions or concerns. If you'd like to work with us, please include your store's name and service area.</p>
            <div className="form-row mb-3">
            <div className="col">
              <input type="text" className="form-control" placeholder="First name"></input>
            </div>
            <div className="col">
              <input type="text" className="form-control" placeholder="Last name"></input>
            </div>
          </div>
          <div className="form-row mb-3">
            <div className="col">
              <input type="text" className="form-control" placeholder="Email"></input>
            </div>
          </div>
          <div className="form-row mb-3">
            <div className="col">
              <textarea type="text" className="form-control" placeholder="What's on your mind..."></textarea>
            </div>
          </div>
          <button className="btn btn-dark mt-1" type="submit">Send</button>
          <div className='mt-4'>
            <small className='text-muted'>Moto Part Picker</small>
            <small className="d-block mb-3 text-muted">© 2019</small>
          </div>
          </form>
        </div>
      </div>
    </div>

    );
  }

export default Contact;
