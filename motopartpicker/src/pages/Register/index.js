import React, { Component } from 'react';
import { withFirebase } from '../../firebase';
import { connect } from 'react-redux';
import { isAttemptingToLogin } from '../../actions';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import isEmail from 'validator/lib/isEmail';
library.add(faGoogle, faFacebook);

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { errorState: null, isAttemptingToLogin: false }
  }

  async createUser() {
    const { firstName, lastName, email, password, passwordCopy } = this.refs;
    const { firebase } = this.props;

    if (firstName.value && lastName.value && email.value && password.value && passwordCopy.value) {
      if (passwordCopy.value !== password.value) {
        this.setState({errorState: 'passwords-do-not-match'})
        return;
      }
      if (!isEmail(email.value)) {
        this.setState({errorState: 'improper-email'})
        return;
      }
      // on success
      await firebase.doCreateUserWithEmailAndPassword(email.value, password.value);
    } else {
      this.setState({errorState: 'not-filled-out'})
      return;
    }
  }

  async loginWithGoogle() {
    const { firebase, isAttemptingToLogin } = this.props;
    isAttemptingToLogin();
    await firebase.doLoginWithGoogle();
  }

  renderError() {
    const { errorState } = this.state;
    let message = '';
    if (errorState) {
      if (errorState === 'not-filled-out') {
        message = 'Please fill out the entire form!';
      }
      if (errorState === 'passwords-do-not-match') {
        message = 'Hey, your passwords do not match!';
      }
      if (errorState === 'improper-email') {
        message = 'Please enter a proper email Ex. You@Internet.com';
      }
      return <div className='error-state-container mt-3'>{message}</div>
    }
  }

  renderAttemptingToLogin() {
    return (
      <div className='attempting-to-login-container'>
        Logging In...
      </div>
    )
  }

  render() {
    const { root } = this.props;
    if (root.isAttemptingToLogin) {
      return this.renderAttemptingToLogin();
    }
    return (
      <div className='register-page-container container'>
        <div className='row mt-3 mb-5'>
          <div className='col-md-6 offset-md-3'>
            <div className="form-signin mt-5">
              <h1 className="h3 mb-2 font-weight-normal lead-text">Register</h1>
              <p className='mb-4'>Create an account to save your part lists.</p>
              <div className='mb-2'>
                <small className='mr-2'>Sign up using </small>
                <button onClick={() => this.loginWithGoogle()}>
                  <FontAwesomeIcon icon={["fab", "google"]} size='lg' />
                </button>
                <button><FontAwesomeIcon icon={["fab", "facebook"]} size='lg' /></button>
              </div>
              <div className='border-top mt-2'></div>
              {this.renderError()}
                <div className="form-row mb-3 mt-4">
                <div className="col">
                  <input type="text" ref="firstName" className="form-control" placeholder="First name"></input>
                </div>
                <div className="col">
                  <input type="text" ref="lastName" className="form-control" placeholder="Last name"></input>
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input type="email" ref="email" className="form-control" placeholder="Email"></input>
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input type="password" ref="password" className="form-control" placeholder="Password"></input>
                </div>
              </div>
              <div className="form-row mb-3">
                <div className="col">
                  <input type="password" ref="passwordCopy" className="form-control" placeholder="Password (Again)"></input>
                </div>
              </div>
              <button onClick={() => this.createUser()} className="btn btn-dark mt-1">Submit</button>
              <div className='mt-4'>
                <small className='text-muted'>Moto Part Picker</small>
                <small className="d-block mb-3 text-muted">© 2019</small>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
 }
}

const mapStateToProps = (state) => {
  return {
    root: state.root
  }
}

const dispatchToProps = {
  isAttemptingToLogin
}

export default connect(mapStateToProps, dispatchToProps)(withFirebase(Register));
