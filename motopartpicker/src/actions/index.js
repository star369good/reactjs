import { push } from 'connected-react-router'

export const loadSavedBuildAndShowBuild = (build) => (dispatch) => {
  dispatch(loadBuild(build));
  dispatch(push('/build'));
};

export const loadBuild = (build) => ({
  type: 'LOAD_BUILD',
  build
});

export const loginUser = (user) => ({
  type: 'LOGIN_USER',
  user
});

export const isAttemptingToLogin = () => ({
  type: 'IS_ATTEMPTING_TO_LOGIN'
});

export const signOutUser = () => ({
  type: 'SIGN_OUT_USER'
});

export const onPartSelection = (part, selection) => ({
  type: 'PART_SELECTION',
  part,
  selection
});

export const onCancelPartSelection = (part) => ({
  type: 'CANCEL_PART_SELECTION',
  part
});

