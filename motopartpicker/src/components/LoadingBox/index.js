import React from 'react';

const LoadingBox = (props) => {
  return (
    <div className='loading-container container'>
      Loading ...
    </div>
  );
}

export default LoadingBox;
