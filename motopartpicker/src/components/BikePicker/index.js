import React from 'react';
import { connect } from 'react-redux';
import './BikePicker.css';
import axios from 'axios';
import { addSearchQueries } from '../../reducers/ApiStore';
import { roadyears } from './roadyears.json';
import { dirtyears } from './dirtyears.json';
import { types } from './types.json';

export class BikePicker extends React.Component {
  constructor(props) {
    super(props);
    const bikeTypeId = 0;
    const yearId = 1;
    const modelId = 1;
    const makeId = 1;
    // const { bikeTypeId, yearId, modelId, makeId } = this.props.apiStore;
    this.state = {
      bikeTypeId,
      yearId,
      modelId,
      makeId,
      years: [],
      makes: [],
      models: [],
      parts: [],
      url: null,
    };
    this.url = 'http://134.209.67.225/api/my-bike?type=';
    this.getMakes = this.getMakes.bind(this);
  }
  
  renderTypesList() {
    return (
      <select ref='mppTypesSelect' onChange={(evt) => this.typeSelected(evt)} className='form-control' defaultValue='0'>
        <option value='0' disabled>Type</option>
        { types.map((types, idx) => <option key={idx}>{ types.type }</option>) }
      </select>
    )
  }

  renderYearsList() {
    return (
      <select ref='mppYearsSelect' onChange={(evt) => this.yearSelected(evt)} className='form-control' defaultValue='1'>
        <option value='1' disabled>Year</option>
        { this.state.years.map((years, idx) => <option key={idx}>{ years.year }</option>) }
      </select>
    )
  }

  renderMakesList() {
    return (
      <select ref='mppMakesSelect' onChange={(evt) => this.makeSelected(evt)} className='form-control' defaultValue='1'>
        <option value='1' disabled>Make</option>
        { this.state.makes.map((makes, idx) => <option key={idx}>{ makes.make_name }</option>) }
      </select>
    )
  }

  renderModelsList() {
    return (
      <select ref='mppModelSelect' onChange={(evt) => this.modelSelected(evt)} className='form-control' defaultValue='1'>
        <option value='1' disabled>Model</option>
        { this.state.models.map((models, idx) => <option key={idx}>{ models.model_name }</option>) }
      </select>
    )
  }

  typeSelected() {
    const typeSelect = this.refs.mppTypesSelect;
    const bikeType =  typeSelect.options[typeSelect.selectedIndex].value;
    const bikeTypeId = types.find(item => item.type === bikeType).id;
    const years = (bikeType === 'Dirt Bike') ? dirtyears : roadyears;
    this.setState({ 
      bikeTypeId, 
      years: years.reverse() 
    });
  }

  yearSelected() {
    const yearSelect = this.refs.mppYearsSelect;
    const selectedYear =  yearSelect.options[yearSelect.selectedIndex].value;
    const year = this.state.years.find((item) => {
      if (item.year === parseInt(selectedYear)) {
        return item.id;
      }
    });
    this.setState({yearId: year.id});
    this.getMakes(year.id);
  }

  makeSelected() {
    const makeSelect = this.refs.mppMakesSelect;
    const selectedMake = makeSelect.options[makeSelect.selectedIndex].value;
    const make = this.state.makes.find((make) => {
      if (make.make_name === selectedMake) {
        return make.id;
      }
    });
    this.setState({makeId: make.id})
    this.getModels(make.id);
  }

  modelSelected() {
    const modelSelect = this.refs.mppModelSelect;
    const selectedModel =  modelSelect.options[modelSelect.selectedIndex].value;
    const model = this.state.models.find((model) => {
      if (model.model_name === selectedModel) {
        return model.id;
      }
    });
    if(model) this.setState({modelId: model.id});
  }

  getMakes(yearId) {
    const { bikeTypeId } = this.state;
    axios.get(this.url + bikeTypeId + '&year=' + yearId)
    .then(res => {
      res.data.makes
      this.setState({makes : res.data.makes, yearId });
    });
  }

  getModels(makeId) {
    const { bikeTypeId, yearId } = this.state;
    axios.get(this.url + bikeTypeId + '&year=' + yearId + '&make=' + makeId)
    .then(res => {this.setState({models : res.data.models, makeId})});
  }

  storeQueries() {
    const { bikeTypeId, yearId, makeId, modelId } = this.state;
    this.props.addSearchQueries({bikeTypeId, yearId, makeId, modelId});
    this.props.history.replace('/build');
  }

  // async getPartsFromApi(modelId) {
  //   const { bikeTypeId, yearId, makeId } = this.state;
  //   const parts = await Api.getParts(this.url, bikeTypeId, yearId, makeId, modelId);
  //   this.setState({parts, modelId});
  //   console.log(parts, '!');
  // }

  render() {
    return (
      <div className='container bike-picker-dropdown-container'>
        <div className='form-row'>
          <div className='col-sm-2 offset-md-1 mobile-view-select'>
            { this.renderTypesList() }
          </div>
          <div className='col-sm-1 mobile-view-select'>
            { this.renderYearsList() }
          </div>
          <div className='col-sm-2 mobile-view-select'>
            { this.renderMakesList() }
          </div>
          <div className='col-sm-3 mobile-view-select'>
            { this.renderModelsList() }
          </div>  
          <div className='col-sm-2 mobile-view-select'>
            <button type='button' onClick={() => this.storeQueries()} 
              className='btn btn-primary button-width'>
              Search Parts
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiStore: state.apiStore
  }
}

const mapDispathToProps = {
  addSearchQueries
}

export default connect(mapStateToProps, mapDispathToProps)(BikePicker);
