import React from 'react';
import './LoadingSpinner.css';

const LoadingSpinner = (props) => {
  return (
    <div className='loading-spinner-container'>
      <div className="loading-spinner" />
    </div>
  )
};

export default LoadingSpinner;