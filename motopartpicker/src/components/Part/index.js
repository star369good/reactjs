import React, { Component } from 'react';
import './Part.css';

class Part extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const part = this.props.part;
    const firstPrice = part.prices[0];
    return (
      <tr>
        <td className=''><button className="btn btn-dark btn-sm" onClick={() => this.props.onSelect()}>Add</button></td>
        <td className=''><img id='img' src={firstPrice.image} /></td>
        <td className=''>{part.title}</td>
        <td className=''>${firstPrice.sale_price || firstPrice.price}</td>
        <td className=''><a className="link" href={firstPrice.link} target="_blank">{(firstPrice.supplier_id == 1) ? 'RevZilla ' : part.brand}</a></td>
      </tr>
    );
  }
}

export default Part;
