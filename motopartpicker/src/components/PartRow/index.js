import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import parts from '../../data/data.json';
import './PartRow.css';

class PartRow extends Component {
  constructor(props) {
    super(props);
    this.parts = parts;
  }

  onPartSelectionView() {
    this.props.onPartSelectionView(this.props.partType);
  }

  getPartSelectionButton() {
    return (
      <button className="btn btn-dark btn-sm" 
        onClick={() => this.onPartSelectionView()}>Choose</button>
    );
  }

  getRenderTr(buyButton, cancelButton, sel, cols, display, isShow, index){
    return (
      <tr key={index} data-show={`${isShow}`}>
        {(index==0) ? <td rowSpan={`${cols}`}>{display}</td> : null}
        <td>{sel.product}</td>
        <td className='mobileTableCell'></td>
        <td className='mobileTableCell'></td>
        <td className='mobileTableCell'></td>
        <td className='mobileTableCell'></td>
        <td className='mobileTableCell'>{(sel.price) ? '$'+sel.price : null}</td>
        <td className='mobileTableCell'><a href={sel.productLink} target='_blank' className='link'>{sel.vendor}</a></td>
        <td>{(sel.price) ? buyButton : null}</td>
        <td>{(sel.price) ? cancelButton : null}</td>
      </tr>
    )
  }

  render() {
    const { display, selection, isShow } = this.props;
    let cnt = (selection) ? selection.length : 0;
    let sels = [];
    for(let i = 0; i < cnt; i++) sels[i] = {...selection[i], index: i};
    sels[cnt] = {product: this.getPartSelectionButton(), price: null, productLink: null, vendor: null, index: cnt};
    const cancelButton = <button className="btn cancel-button btn-sm btn-dark" onClick={this.props.onCancel}> X </button>
    const buyButton = <a href={(selection) ? selection.productLink : null} target="_blank"><button className="btn btn-primary btn-sm" onClick={this.props.onBuyButton}>Buy</button></a>
    return sels.map((sel,index) => {
      return this.getRenderTr(buyButton, cancelButton, sel, cnt+1, display, isShow, index);
    });
  }
}

export default PartRow;
