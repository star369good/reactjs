import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from "react-router-dom";
import { loginUser, signOutUser } from '../../actions';
import MainPage from '../../pages/MainPage';
import BuildList from '../../pages/BuildList';
import BuildCreator from '../../pages/BuildCreator/index';
import Header from '../Header';
import Footer from '../Footer';
import Register from '../../pages/Register/index';
import SignIn from '../../pages/SignIn/index';
import AccountPage from '../../pages/AccountPage/index';
import Contact from '../../pages/Contact/index';
import withTracker from '../../withTracker';
import PartnerList from '../../pages/PartnerList/index';
import PrivacyPolicy from '../../pages/PrivacyPolicy/index';
import { withFirebase } from '../../firebase';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fal } from '@fortawesome/pro-light-svg-icons';
import { fas } from '@fortawesome/pro-solid-svg-icons';
library.add(fas, fal);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.listener = this.props
      .firebase
      .auth
      .onAuthStateChanged(user => this.onAuthStateChanged(user));
  }
  componentWillUnmount() {
    // Fires method that deletes onAuthStateChanged listener
    this.listener();
  }

  onAuthStateChanged(user) {
    this.props.history.push('/');
    if (user) {
      this.props.loginUser(user);
    } else {
      this.props.signOutUser();
    }
  }

  render() {
    return (
      <div className="app-container">
        <Header root={this.props.root}/>
        <Switch>
          <Route exact path="/" component={withTracker(MainPage)} />
          <Route exact path="/buildlist" component={withTracker(BuildList)} />
          <Route exact path="/build" component={withTracker(BuildCreator)} />
          <Route exact path="/register" component={withTracker(Register)} />
          <Route exact path="/signin" component={withTracker(SignIn)} />
          <Route exact path="/account" component={withTracker(AccountPage)} />
          <Route exact path="/partnerlist" component={withTracker(PartnerList)} />
          <Route exact path="/privacypolicy" component={withTracker(PrivacyPolicy)} />
          <Route exact path="/contact" component={withTracker(Contact)} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    root: state.root
  }
}

const dispatchToProps = {
  loginUser,
  signOutUser
}

export default withRouter(connect(mapStateToProps, dispatchToProps)(withFirebase(App)));
