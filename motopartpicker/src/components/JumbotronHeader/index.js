import React from 'react';
import './JumbotronHeader.css';

const JumbotronHeader = (props) => {
  return (
    <div className={'jumbotron-header ' + (props.styleName || '')}>
      <div className="jumbo-text">{props.text}</div>
    </div>
  )
};

export default JumbotronHeader;