import React, { Component } from 'react';
import './SelectionView.css';
import Part from '../Part';
import JumbotronHeader from '../JumbotronHeader';
import { Redirect } from 'react-router-dom';
import { categoriesMap } from '../../data/categories';
import * as API from '../../utils/api';

class SelectionView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasLoaded: false
      , hasContents: false
    }
  }

  componentDidMount() {
    this.getPartsFromApi()
  }

  async getPartsFromApi() {
    const { apiStore, selecting } = this.props;
    const { bikeTypeId, yearId, makeId, modelId } = apiStore;
    try {
      const parts = await API.getPartsForCategory(bikeTypeId, yearId, makeId, modelId, selecting);
      this.setState({ parts, hasLoaded: true, hasContents: (parts.length > 0) });
    } catch(err) {
      console.log(err)
    }
  }

  renderParts() {
    const { parts } = this.state;
    const { selecting, selectedParts } = this.props;
    const selection = selectedParts[selecting].selection;
    return parts
      .map((item, idx) => {
        for(let i in selection){
          if(selection[i].id == item.id) return null;
        }
        return (true) ? <Part key={idx} part={item} onSelect={() => this.props.onSelect(item)} /> : null;
      });
  }

  render() {
    const { selecting } = this.props;
    const { hasLoaded, hasContents } = this.state;
    const productName = categoriesMap[selecting].name;

    if (!selecting) {
      return <Redirect to='/build' />
    }

    if (!hasLoaded) {
      return (
        <div className="list-area section-container selection-view-container">
          Loading...
        </div>
      );
    }

    return (
      <div className="list-area section-container selection-view-container">
      <JumbotronHeader text={`Choose ${productName} Parts`}/>
        <div className="container-fluid section-container">
          <div className="row h-100">
            <div className="col-md-3 filter-column">
              <h5 className="mt-5">Filters</h5>
              <hr/>
            </div>
            <div className="col-md-9 bg-white">
              <div className='row table-header'>
                <div className="">Parts</div>
                <button className="btn btn-dark btn-sm" onClick={() => this.props.onCancel(hasContents)}>Go Back</button>
              </div>
              <hr/>
              <table className='table mt-2'>
                <thead>
                  <tr>
                    <th>Add to List</th>
                    <th>{productName}</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Where</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderParts()}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectionView;
