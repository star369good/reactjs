import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './Header.css';
import logo from '../../assets/mpp-logo.png';
import HeaderButton from './HeaderButton';
import { withFirebase } from '../../firebase';

const SignUpButtons = () => {
  return (
    <React.Fragment>
      <small id='desktop-only-text' className='navbar-text'>
        <span className='mr-2'>Don't have an account yet?</span>
        <Link className='link' to ='/register'>Register</Link>
      </small>
      <HeaderButton
        text='Sign In'
        linkTo='/signin'
        icon='user'
        size='sm'
      />
    </React.Fragment>
  );
}

const LoggedInButtons = (props) => {
  return (
    <React.Fragment>
      <HeaderButton
        text='Account'
        linkTo='/account'
        icon='user'
        size='sm'
      />
      <HeaderButton
        text='Sign Out'
        action={props.signOut}
        icon='minus'
        size='sm'
      />
    </React.Fragment>
  );
};

const Header = (props) => {
  const { isLoggedIn, user } = props.root;
  const { doSignOut } = props.firebase;
  return (
    <div className='container-fluid p-0'>
      <nav className='navbar'>
        <Link to='/'>
          <img id='mpp-icon' className='mr-3' src={ logo } />
        </Link>
        <ul className='nav mr-auto mt-lg-0'>
          <HeaderButton
            text='Create a Part List'
            linkTo='/build'
            icon='plus'
            size='sm'
          />
          <HeaderButton
            text='My Saved Lists'
            linkTo='/buildlist'
            icon='list'
            size='sm'
          />
        </ul>
        {isLoggedIn ? <LoggedInButtons signOut={doSignOut} /> : <SignUpButtons />}
      </nav>
  </div>
  );
}


export default (withFirebase(Header));
