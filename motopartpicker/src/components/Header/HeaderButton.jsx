import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const HeaderButton = (props) => {

  if (props.action) {
    return (
      <div onClick={props.action} className="btn btn-outline-dark btn-sm ml-3">
        {props.icon ? <FontAwesomeIcon icon={props.icon} size={props.size} /> : null }
        <span id='desktop-only-buttons'>{props.text}</span>
      </div>
    );
  }

  return (
    <Link to={props.linkTo}>
      <div className="btn btn-outline-dark btn-sm ml-3">
        {props.icon ? <FontAwesomeIcon icon={props.icon} size={props.size} /> : null }
        <span id='desktop-only-buttons'>{props.text}</span>
      </div>
    </Link>
  );

}


export default HeaderButton;
