import React from 'react';
import './Footer.css';
import { Link } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
library.add(faInstagram);

const Footer = (props) => {
  return (
    <div className='container border-top'>
      <footer className='pt-2 my-md-5'>
        <div className='row'>
          <div className='col-12 col-md'>
            Moto Part Picker
            <small className='d-block mb-3 text-muted'>© 2019</small>
            <div className='mt-3'><small className='text-primary'>Follow Us</small>
            <p className='mt-2'>
              <a href='https://www.instagram.com/motopartpicker' target='_blank' rel="noopener noreferrer"><FontAwesomeIcon icon={["fab", "instagram"]} size='lg' /></a>
            </p>
            </div>
          </div>
          <div className='col-12 col-md'>
            <h5 className='lead-text'>About</h5>
            <ul className='list-unstyled text-small'>
              <li><Link className='text-muted' to='/contact'>Work with us</Link></li>
              <li><Link className='text-muted' to='/partnerlist'>Partner List</Link></li>
              <li><Link className='text-muted' to='/privacypolicy'>Privacy</Link></li>
            </ul>
          </div>
          <div className='col-12 col-md'>
            <h5 className='lead-text'>Customer Service</h5>
            <ul className='list-unstyled text-small'>
              <li><Link className='text-muted' to='/register'>Register</Link></li>
              <li><Link className='text-muted' to='/contact'>Contact Us</Link></li>
            </ul>
          </div>
          <div className='col-12 col-md'>
            <h5 className='lead-text'>Feedback</h5>
            <p>Tell us what you love or what we need to fix.</p>
            <Link className='btn btn-dark mt-1' to='/contact'>Leave Feedback</Link>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
