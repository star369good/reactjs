import React from "react";

class SignalRConnector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          driverLinesText: '',
          driverLines: [],
          driverNames: [],
          driverStats: []
        };
        this.handleTextArea = this.handleTextArea.bind(this);
        this.handleClickTextArea = this.handleClickTextArea.bind(this);
        this.handleDriverLines = this.props.handleDriverLines;
    }
    handleTextArea(){
      var newVal = this.myInput.value;
      this.setState({'driverLinesText' : newVal});
    }
    handleClickTextArea(){
    //   var newVal = this.myInput.value;
      var newValStore = localStorage.getItem('driverLines');
      var newValStoreStat = localStorage.getItem('driverStats');
      this.setState({'driverLinesText' : newValStore});
      
    //  console.log("newVal of localStorage is " + newValStore);
    //  console.log("newValStoreStat of localStorage is " + newValStoreStat);

    //  if(newVal && newVal.length > 0){
      if(newValStore && newValStore.length > 0){
        // let tempVal = JSON.parse(newVal);
        let tempVal = JSON.parse(newValStore);
        var tempResult1 = (tempVal.length > 0) ? 
            tempVal.filter((driver) => {
                    if(driver == null) return false;
                    else return driver;
                }) : [];
        var tempNames1 = tempResult1.map((driver, index) => {
                return {
                    label : driver.Driver.FullName,
                    value : index
                };
            });

        var tempResult3 = tempResult1.map((driver) => {
          if(driver.DriverPosition.Value === "1"){
            driver.GapToLeader = "0";
            driver.IntervalToPositionAhead = "0";
          }
          else{
            driver.GapToLeader = ((driver.GapToLeader === undefined) ? (driver.TimeDiffToFastest): (driver.GapToLeader));
            driver.IntervalToPositionAhead = ((driver.IntervalToPositionAhead === undefined) ? (driver.TimeDiffToPositionAhead) : (driver.IntervalToPositionAhead));
          }
          return driver;
        });
        
        this.setState({
            'driverLines' : tempResult3,
            'driverNames' : tempNames1 
        });
        // window.driverLines = tempResult;
        // window.driverNames = tempNames;
        
      }
    // else{
    //     console.log("window drivers ");
    //     console.log(window.driverLines);
    //     if(window.driverLines && window.driverLines.length > 0){
    //         this.setState({'driverLines' : window.driverLines});  
    //         console.log("window drivers => driverLines.");
    //     }
    //     if(window.driverNames && window.driverNames.length > 0){
    //         this.setState({'driverNames' : window.driverNames});  
    //     }
    // }

      if(newValStoreStat && newValStoreStat.length > 0){
        let tempVal = JSON.parse(newValStoreStat);
        var tempResult2 = (tempVal.length > 0) ? 
            tempVal.filter((driver) => {
                    if(driver == null) return false;
                    else return driver;
                }) : [];
        

        // driver stats => driver lines
        // this.state.driverLines.forEach((driver) => {
        //   if(driver.GapToLeader === undefined) driver.GapToLeader = tempResult2.find((tempDriver) => { return tempDriver. === driver.Driver.RacingNumber })
        // });

        this.setState({ 'driverStats' : tempResult2 });
      }

      if((newValStore && newValStore.length > 0) || (newValStoreStat && newValStoreStat.length > 0)){
        this.handleDriverLines(tempResult1, tempNames1, tempResult2);
      }

      console.log('signalr connector handle ');
      console.log(this.state.driverLines);
      console.log(this.state.driverStats);
    }
    componentDidMount(){
        this.handleClickTextArea();
    }
    render(){
        return (
            <>
                <textarea 
                    name="state-text"
                    id="state_textarea"
                    onChange={this.handleTextArea}
                    onClick={this.handleClickTextArea}
                    ref={(myInput) => { this.myInput = myInput; }}
                    value={(this.state.driverLinesText)}
                    style={{display : 'none'}}
                />
            </>
        );
    }
}

export default SignalRConnector;