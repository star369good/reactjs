import React from "react";
import ConnectAPI from "../../connectAPI";

// reactstrap components
import {
    Row,
    Col,
    Card,
    // CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Table
  } from "reactstrap";

  class RightSidebar extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        driverValues : [],
        driverStats : [],
        sortedDrivers : [],
        circuitId : 2,
        circuit : {},
        lapCurrent : 0
      };
    }
    componentDidMount() {
        var conn = new ConnectAPI();
        conn.getFetch('circuit', 'id='+this.state.circuitId)
          .then(data => {
            data = JSON.parse(data);
            if(data.length > 0){
                this.setState({ circuit : data[0] });
            }
          });
    }
    componentWillReceiveProps(props) {
        this.setState({ 
            driverValues : props.driverLines,
            driverStats : props.driverStats,
            sortedDrivers : this.getSortedDrivers(props.driverStats, props.driverLines),
            lapCurrent : this.getLapCurrent(props.driverLines)
        });
    }
    parseTime(t) {
        var mss = t.split('.');
        var s = mss[0].split(':');
        var ret = 0;
        var n = 0;
        for (n; n < s.length; n++)
            ret = ret * 60 + parseInt(s[n]);
        ret *= 1000;
        if (mss.length > 1)
            ret += parseInt(mss[1]);
        return new Date(ret);
    }
    getLapCurrent(drivers){
        return Math.max.apply(Math, drivers.map((driver) => { return driver.NumberOfLaps; }))
    }
    getDriverName(number, driverValues){
        let result = 'Number'+number;
        if(driverValues.length > 0){
            driverValues.forEach((driver) => {
                if(parseInt(driver.Driver.RacingNumber) === parseInt(number)) result = driver.Driver.BroadcastName;
            });
        }
        return result;
    }
    getSortedDrivers(driverStats, driverValues){
        let currentLists = this.state.sortedDrivers;

        if(driverStats.length > 0){
            let tempLists = driverStats.map((driver) => {
                return {
                    bestLapTime: driver.PersonalBestLapTime.Value,
                    number : driver.Driver.RacingNumber,
                    name : this.getDriverName(driver.Driver.RacingNumber, driverValues),
                    position : driver.PersonalBestLapTime.Position,
                    lap : driver.PersonalBestLapTime.Lap
                };
            });

            tempLists.forEach((driver) => {
                let flag = true;
                if(currentLists.length > 0) currentLists.forEach((list) => {
                    if(driver.number === list.number && driver.lap === list.lap) flag = false;
                });

                if(flag){
                    currentLists.push(driver);
                }
            });

            let resultLists = [];
            resultLists = currentLists.sort((a, b) => {
                return this.parseTime(a.bestLapTime) - this.parseTime(b.bestLapTime);
            });

            if(resultLists.length > 10) resultLists = resultLists.slice(0, 10);

            return resultLists;
        }

        return currentLists;
    }
    render(){
          return (
              <> 
                <Card className="card-stats">
                    <CardBody>
                        <Row>
                            <Col xs="3">
                                <div className="info-icon text-center icon-danger">
                                    <i className="tim-icons icon-support-17" />
                                </div>
                            </Col>
                            <Col xs="9">
                                <div className="numbers">
                                    <p className="card-category">LAP</p>
                                    <CardTitle tag="h3">{this.state.circuit.laps2}</CardTitle>
                                </div>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <hr />
                        <Row className="stats">
                            <i className="tim-icons icon-tag" />
                            <Col sm="4">REMAINING LAP</Col>
                            <Col sm="6">{((!isNaN(parseInt(this.state.circuit.laps2))) ? parseInt(this.state.circuit.laps2) : 0) - ((!isNaN(parseInt(this.state.lapCurrent))) ? parseInt(this.state.lapCurrent) : 0)}</Col>
                        </Row>
                    </CardFooter>
                </Card>
                <Card className="card-stats">
                    <CardBody>
                        <Row>
                            <Col xs="3">
                                <div className="info-icon text-center icon-warning">
                                    <i className="tim-icons icon-globe-2" />
                                </div>
                            </Col>
                            <Col xs="9">
                                <div className="numbers">
                                    <p className="card-category">CIRCUIT</p>
                                    <CardTitle tag="h3">{this.state.circuit.title}</CardTitle>
                                </div>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <hr />
                        <Row className="stats">
                            <i className="tim-icons icon-spaceship" />
                            <Col sm="4">COUNTRY </Col>
                            <Col sm="6"> {this.state.circuit.country} </Col>
                        </Row>
                        <Row className="stats">
                            <i className="tim-icons icon-calendar-60" />
                            <Col sm="4">DATE </Col>
                            <Col sm="6"> {this.state.circuit.date} </Col>
                        </Row>
                        <Row className="stats">
                            <i className="tim-icons icon-square-pin" />
                            <Col sm="4">LENGTH </Col>
                            <Col sm="6"> {this.state.circuit.length} km </Col>
                        </Row>
                    </CardFooter>
                </Card>
                <Card className="card-stats">
                    <CardBody>
                        <Row>
                            <Col xs="3">
                                <div className="info-icon text-center icon-primary">
                                    <i className="tim-icons icon-trophy" />
                                </div>
                            </Col>
                            <Col xs="9">
                                <div className="numbers">
                                    <p className="card-category">TOP 10</p>
                                    <CardTitle tag="h3">BEST LAPTIME RANKING</CardTitle>
                                </div>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <hr />
                        <Row className="stats">
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>P</th>
                                        <th>NAME</th>
                                        <th>TIME</th>
                                        <th>LAP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {(this.state.sortedDrivers.length > 0) ? (this.state.sortedDrivers.map((driver, index) => (
                                        <tr key={index}>
                                            <td>{driver.position}</td>
                                            <td>{driver.name}</td>
                                            <td>{driver.bestLapTime}</td>
                                            <td>{driver.lap}</td>
                                        </tr>
                                    ))) : null}
                                </tbody>
                            </Table>
                        </Row>
                    </CardFooter>
                </Card>
            </>);
    }
  }

  export default RightSidebar;