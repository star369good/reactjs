import React from "react";
import Select from "react-select";

// reactstrap components
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  // CardFooter,
  CardTitle,
  Form,
  FormGroup,
  Button,
  Label,
  Table
} from "reactstrap";

import RightSidebar from "components/Sidebar/RightSidebar.jsx";

class Compare extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      singleSelectDriverValues: [],
      driverValues: [],
      singleSelectDriverA: null,
      singleSelectDriverB: null,
      singleSelectDriverP: null,
      driverA: {},
      driverB: {},
      driverP: {},
      compareAP : this.getEmptyParams(),
      comparePB : this.getEmptyParams(),
      singleSelectDriverAA: null,
      singleSelectDriverBB: null,
      singleSelectDriverPP: null,
      driverAA: {},
      driverBB: {},
      driverPP: {},
      compareAAPP : this.getEmptyParams(),
      comparePPBB : this.getEmptyParams()
    };
  }
  componentDidMount(){
  }
  componentWillReceiveProps(props) {
    this.setState({ driverValues : props.driverLines, singleSelectDriverValues: props.driverNames });
  }
  handleChangeSelectDriver(value, key){
    if(key === 'A'){
      this.setState({ 
        singleSelectDriverA: value, 
        driverA: this.state.driverValues[value.value], 
        compareAP: this.getCompareTwoDrivers(this.state.driverP, this.state.driverValues[value.value]) 
      });
    }
    else if(key === 'B'){
      this.setState({ 
        singleSelectDriverB: value, 
        driverB: this.state.driverValues[value.value], 
        comparePB: this.getCompareTwoDrivers(this.state.driverP, this.state.driverValues[value.value]) 
      });
    }
    else if(key === 'P'){
      this.setState({ 
        singleSelectDriverP: value, 
        driverP: this.state.driverValues[value.value], 
        compareAP: this.getCompareTwoDrivers(this.state.driverValues[value.value], this.state.driverA), 
        comparePB: this.getCompareTwoDrivers(this.state.driverValues[value.value], this.state.driverB) 
      });
    }
    else if(key === 'AA'){
      this.setState({ 
        singleSelectDriverAA: value, 
        driverAA: this.state.driverValues[value.value], 
        compareAAPP: this.getCompareTwoDrivers(this.state.driverPP, this.state.driverValues[value.value]) 
      });
    }
    else if(key === 'BB'){
      this.setState({ 
        singleSelectDriverBB: value, 
        driverBB: this.state.driverValues[value.value], 
        comparePPBB: this.getCompareTwoDrivers(this.state.driverPP, this.state.driverValues[value.value]) 
      });
    }
    else if(key === 'PP'){
      this.setState({ 
        singleSelectDriverPP: value, 
        driverPP: this.state.driverValues[value.value], 
        compareAAPP: this.getCompareTwoDrivers(this.state.driverValues[value.value], this.state.driverAA), 
        comparePPBB: this.getCompareTwoDrivers(this.state.driverValues[value.value], this.state.driverBB) 
      });
    }
  }
  parseTime(t) {
      var mss = t.split('.');
      var s = mss[0].split(':');
      var ret = 0;
      var n = 0;
      for (n; n < s.length; n++)
          ret = ret * 60 + parseInt(s[n]);
      ret *= 1000;
      if (mss.length > 1)
          ret += parseInt(mss[1]);
      return new Date(ret);
  }
  getEmptyParams(){
    return {
      GAP : 'NULL',
      INT : 'NULL',
      S1 : 'NULL',
      S2 : 'NULL',
      S3 : 'NULL',
      TIME : 'NULL'
    };
  }
  getCompareTwoDrivers(driverOne, driverTwo){
    if(driverOne != null && driverTwo != null && Object.keys(driverOne).length > 0 && Object.keys(driverTwo).length > 0){
      return {
        GAP : this.getCompareTwoParameters(driverOne.GapToLeader, driverTwo.GapToLeader),
        INT : this.getCompareTwoParameters(driverOne.IntervalToPositionAhead, driverTwo.IntervalToPositionAhead),
        S1 : this.getCompareTwoParameters(driverOne.Sectors[0].Value, driverTwo.Sectors[0].Value),
        S2 : this.getCompareTwoParameters(driverOne.Sectors[1].Value, driverTwo.Sectors[1].Value),
        S3 : this.getCompareTwoParameters(driverOne.Sectors[2].Value, driverTwo.Sectors[2].Value),
        TIME : this.getCompareTwoParameters(this.parseTime(driverOne.LastLapTime.Value)/1000, this.parseTime(driverTwo.LastLapTime.Value)/1000)
      };
    }
    return this.getEmptyParams();
  }
  getCompareTwoParameters(paramOne, paramTwo){
    let floatOne = parseFloat(paramOne);
    let floatTwo = parseFloat(paramTwo);
    if(isNaN(floatOne) || isNaN(floatTwo)) return "NULL";
    else return (floatOne > floatTwo) ? "+".concat(Math.round((floatOne - floatTwo) * 100) / 100) : Math.round((floatOne - floatTwo) * 100) / 100;
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="9" sm="12">
              <Row>
                <Col md="6" sm="12">
                  <Card>
                    <CardHeader>
                      <CardTitle tag="h3">
                        <i className="tim-icons icon-paper" /> Compare 1 <hr/>
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <Form className="form-horizontal">
                        <Row>
                          <Label md="3">Driver A</Label>
                          <Col md="9">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select primary"
                                classNamePrefix="react-select"
                                name="singleSelectA"
                                value={this.state.singleSelectDriverA}
                                onChange={value => this.handleChangeSelectDriver(value, 'A')}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Select Driver A"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3">Principal</Label>
                          <Col md="9">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select success"
                                classNamePrefix="react-select"
                                name="singleSelectP"
                                value={this.state.singleSelectDriverP}
                                onChange={value => this.handleChangeSelectDriver(value, 'P')}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Select Principal"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3">Driver B</Label>
                          <Col md="9">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select primary"
                                classNamePrefix="react-select"
                                name="singleSelectB"
                                value={this.state.singleSelectDriverB}
                                onChange={value => this.handleChangeSelectDriver(value, 'B')}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Select Driver B"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card>
                    <CardBody>
                      <Row>
                        <Button className="btn-round btn-just-icon ml-auto mr-auto mb-4" color="primary">
                          {(this.state.singleSelectDriverA != null) ? this.state.singleSelectDriverA.label : "Driver A"}
                        </Button>
                      </Row>
                      <Row className="mt-3 mb-3">
                        <Col>
                          <Table responsive>
                            <thead className="text-primary">
                              <tr>
                                <th className="text-center" style={{fontSize:"20px"}}>GAP</th>
                                <th className="text-center" style={{fontSize:"20px"}}>INT</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S1</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S2</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S3</th>
                                <th className="text-center" style={{fontSize:"20px"}}>TIME</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAP.GAP) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAP.GAP) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAP.GAP}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAP.INT) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAP.INT) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAP.INT}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAP.S1) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAP.S1) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAP.S1}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAP.S2) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAP.S2) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAP.S2}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAP.S3) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAP.S3) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAP.S3}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAP.TIME) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAP.TIME) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAP.TIME}
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                        </Col>
                      </Row>
                      <Row>
                        <Button className="btn-just-icon ml-auto mr-auto mb-4" color="success">
                          {(this.state.singleSelectDriverP != null) ? this.state.singleSelectDriverP.label : "Principal"}
                        </Button>
                      </Row>
                      <Row className="mt-3 mb-3">
                        <Col>
                          <Table responsive>
                            <tbody>
                              <tr>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePB.GAP) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePB.GAP) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePB.GAP}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePB.INT) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePB.INT) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePB.INT}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePB.S1) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePB.S1) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePB.S1}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePB.S2) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePB.S2) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePB.S2}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePB.S3) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePB.S3) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePB.S3}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePB.TIME) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePB.TIME) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePB.TIME}
                                </td>
                              </tr>
                            </tbody>
                            <tfoot className="text-primary">
                              <tr>
                                <th className="text-center" style={{fontSize:"20px"}}>GAP</th>
                                <th className="text-center" style={{fontSize:"20px"}}>INT</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S1</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S2</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S3</th>
                                <th className="text-center" style={{fontSize:"20px"}}>TIME</th>
                              </tr>
                            </tfoot>
                          </Table>
                        </Col>
                      </Row>
                      <Row>
                        <Button className="btn-round btn-just-icon ml-auto mr-auto" color="primary">
                          {(this.state.singleSelectDriverB != null) ? this.state.singleSelectDriverB.label : "Driver B"}
                        </Button>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="6" sm="12">
                  <Card>
                    <CardHeader>
                      <CardTitle tag="h3">
                        <i className="tim-icons icon-paper" /> Compare 2 <hr/>
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <Form className="form-horizontal">
                        <Row>
                          <Label md="3">Driver A</Label>
                          <Col md="9">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select primary"
                                classNamePrefix="react-select"
                                name="singleSelectAA"
                                value={this.state.singleSelectDriverAA}
                                onChange={value => this.handleChangeSelectDriver(value, 'AA')}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Select Driver A"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3">Principal</Label>
                          <Col md="9">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select success"
                                classNamePrefix="react-select"
                                name="singleSelectPP"
                                value={this.state.singleSelectDriverPP}
                                onChange={value => this.handleChangeSelectDriver(value, 'PP')}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Select Principal"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="3">Driver B</Label>
                          <Col md="9">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select primary"
                                classNamePrefix="react-select"
                                name="singleSelectBB"
                                value={this.state.singleSelectDriverBB}
                                onChange={value => this.handleChangeSelectDriver(value, 'BB')}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Select Driver B"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card>
                    <CardBody>
                      <Row>
                        <Button className="btn-round btn-just-icon ml-auto mr-auto mb-4" color="primary">
                          {(this.state.singleSelectDriverAA != null) ? this.state.singleSelectDriverAA.label : "Driver A"}
                        </Button>
                      </Row>
                      <Row className="mt-3 mb-3">
                        <Col>
                          <Table responsive>
                            <thead className="text-primary">
                              <tr>
                                <th className="text-center" style={{fontSize:"20px"}}>GAP</th>
                                <th className="text-center" style={{fontSize:"20px"}}>INT</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S1</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S2</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S3</th>
                                <th className="text-center" style={{fontSize:"20px"}}>TIME</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAAPP.GAP) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAAPP.GAP) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAAPP.GAP}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAAPP.INT) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAAPP.INT) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAAPP.INT}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAAPP.S1) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAAPP.S1) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAAPP.S1}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAAPP.S2) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAAPP.S2) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAAPP.S2}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAAPP.S3) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAAPP.S3) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAAPP.S3}
                                </td>
                                <td className="text-center" 
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.compareAAPP.TIME) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.compareAAPP.TIME) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.compareAAPP.TIME}
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                        </Col>
                      </Row>
                      <Row>
                        <Button className="btn-just-icon ml-auto mr-auto mb-4" color="success">
                          {(this.state.singleSelectDriverPP != null) ? this.state.singleSelectDriverPP.label : "Principal"}
                        </Button>
                      </Row>
                      <Row className="mt-3 mb-3">
                        <Col>
                          <Table responsive>
                            <tbody>
                              <tr>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePPBB.GAP) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePPBB.GAP) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePPBB.GAP}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePPBB.INT) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePPBB.INT) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePPBB.INT}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePPBB.S1) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePPBB.S1) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePPBB.S1}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePPBB.S2) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePPBB.S2) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePPBB.S2}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePPBB.S3) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePPBB.S3) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePPBB.S3}
                                </td>
                                <td className="text-center"  style={{borderWidth: '0'}}
                                  ref={element => {
                                    if(element){
                                      if (parseFloat(this.state.comparePPBB.TIME) > 0) element.style.setProperty('color', '#fd5d93', 'important'); 
                                      else if (parseFloat(this.state.comparePPBB.TIME) < 0) element.style.setProperty('color', '#00bf9a', 'important'); 
                                      else element.style.setProperty('color', '#bfe20a', 'important'); 
                                    }
                                     }}
                                >
                                  {this.state.comparePPBB.TIME}
                                </td>
                              </tr>
                            </tbody>
                            <tfoot className="text-primary">
                              <tr>
                                <th className="text-center" style={{fontSize:"20px"}}>GAP</th>
                                <th className="text-center" style={{fontSize:"20px"}}>INT</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S1</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S2</th>
                                <th className="text-center" style={{fontSize:"20px"}}>S3</th>
                                <th className="text-center" style={{fontSize:"20px"}}>TIME</th>
                              </tr>
                            </tfoot>
                          </Table>
                        </Col>
                      </Row>
                      <Row>
                        <Button className="btn-round btn-just-icon ml-auto mr-auto" color="primary">
                          {(this.state.singleSelectDriverBB != null) ? this.state.singleSelectDriverBB.label : "Driver B"}
                        </Button>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
            <Col md="3" sm="12">
              <RightSidebar {...this.props} />
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Compare;
