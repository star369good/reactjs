import React from "react";
import Select from "react-select";
import ReactTable from "react-table";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// import classnames from "classnames";

// reactstrap components
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  FormGroup,
  Input,
  // InputGroup,
  Button,
  Label
} from "reactstrap";

import RightSidebar from "components/Sidebar/RightSidebar.jsx";


class Ghost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      singleSelectDriverValues: [],
      singleSelectDriverIntial: null,
      driverValues: [],
      virtualDriverValues: [],
      singleSelectColorValues: [
        {
          label : "RED",
          value : "#f84c6c"
        },
        {
          label : "PINK",
          value : "#e14eca"
        },
        {
          label : "BLUE",
          value : "#1d8cf8"
        },
        {
          label : "GREEN",
          value : "#00bf9a"
        },
        {
          label : "BROWN",
          value : "#ff8d72"
        }
      ],
      singleSelectColor: null,
      gapToAdd: '',
      virtualDriverName: '',
      tableData: [],
      tableColums: [
        {
          Header: "",
          accessor: "select",
          className: "text-center",
          sortable: false,
          maxWidth: 50
        },
        {
          Header: "P",
          accessor: "pos",
          headerClassName: "text-center",
          className: "text-center",
          sortMethod: (a, b) => this.compareTwoParams(a, b),
          maxWidth: 100
        },
        {
          Header: "NO",
          accessor: "number",
          headerClassName: "text-center",
          className: "text-center",
          sortMethod: (a, b) => this.compareTwoParams(a, b),
          maxWidth: 100
        },
        {
          Header: "NAME",
          accessor: "name",
          headerClassName: "text-center",
          className: ""
        },
        {
          Header: "GAP",
          accessor: "GAP",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "INTERVAL",
          accessor: "INT",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "S1",
          accessor: "S1",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "S2",
          accessor: "S2",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "S3",
          accessor: "S3",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "TIME",
          accessor: "LAPTIME",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "PIT",
          accessor: "PIT",
          headerClassName: "text-center",
          className: "text-center"
        },
        {
          Header: "",
          accessor: "colorValue",
          show: false
        }
      ],
      checkValues: [],
      checkCount: 0,
      alert: null
    };
    this.handleClickADD = this.handleClickADD.bind(this);
    this.handleClickDEL = this.handleClickDEL.bind(this);
    this.handleCheckbox = this.handleCheckbox.bind(this);
    this.deleteVirtualDrivers = this.deleteVirtualDrivers.bind(this);
  }
  compareTwoParams(a, b){
    return parseInt(a) - parseInt(b);
  }
  componentDidMount(){
  }
  componentWillReceiveProps(props) {
    this.setState({ 
      driverValues : props.driverLines, 
      singleSelectDriverValues: props.driverNames, 
      tableData : this.getTableData(props.driverLines, this.state.virtualDriverValues)
    });
  }
  hideAlert = function(){
    this.setState({
      alert: null
    });
  }
  successDelete = function(){
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="Deleted!"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="success"
          btnSize=""
        >
          The selected driver has been deleted.
        </ReactBSAlert>
      )
    });
  }
  successAlert = function(content){
    this.setState({
      alert: (
        <ReactBSAlert
          success
          style={{ display: "block", marginTop: "-100px" }}
          title="SUCCESS"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="success"
          btnSize=""
        >
          {content}
        </ReactBSAlert>
      )
    });
  }
  warningWithConfirmMessage = function(content, keys){
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Are you sure?"
          onConfirm={() => {
            this.deleteVirtualDrivers(keys);
            this.successDelete();
          }}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="success"
          cancelBtnBsStyle="danger"
          confirmBtnText="Yes, delete it!"
          cancelBtnText="Cancel"
          showCancel
          btnSize=""
        >
          {content}
        </ReactBSAlert>
      )
    });
  }
  titleAndTextAlert = function(content){
    this.setState({
      alert: (
        <ReactBSAlert
          warning
          style={{ display: "block", marginTop: "-100px" }}
          title="Warning"
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          confirmBtnBsStyle="success"
          btnSize=""
        >
          {content}
        </ReactBSAlert>
      )
    });
  }
  getTableRow(driver, key, isReal){
    if(isReal){
      return {
        id : key,
        select : (
          <FormGroup check>
            <Label check style={{paddingTop : '15px'}}>
              <Input 
                key={driver.name}
                type="checkbox" 
                name={driver.name}
                // checked={this.state.checkValues[driver.Driver.BroadcastName] || false}
                // defaultChecked={false}
                disabled 
              />
              <span className="form-check-sign" />
            </Label>
          </FormGroup>
        ),
        pos : driver.pos,
        number : driver.number,
        name : driver.name,
        GAP : driver.GAP,
        INT : driver.INT,
        S1 : driver.S1,
        S2 : driver.S2,
        S3 : driver.S3,
        LAPTIME : driver.LAPTIME,
        PIT : driver.PIT,
        colorValue : driver.colorValue
      };
    }
    else{
      let inputKey = driver.name + this.state.checkCount.toString();
      return {
        id : key,
        select : (
          <FormGroup check>
            <Label check style={{paddingTop : '15px'}}>
              <Input 
                key={inputKey}
                type="checkbox" 
                name={driver.name}  
                // checked={this.state.checkValues[driver.name] || false}
                // defaultChecked={false}
                onChange={this.handleCheckbox}
              />
              <span className="form-check-sign" />
            </Label>
          </FormGroup>
        ),
        pos : driver.pos,
        number : driver.number,
        name : driver.name,
        GAP : driver.GAP,
        INT : driver.INT,
        S1 : driver.S1,
        S2 : driver.S2,
        S3 : driver.S3,
        LAPTIME : driver.LAPTIME,
        PIT : driver.PIT,
        colorValue : driver.colorValue
      };
    }
  }
  getTableData(realDrivers, virtualDrivers){
    console.log("get table data");
    console.log(realDrivers);
    console.log(virtualDrivers);


    let index = 0;
    let findPos, findGAP, temp, tempNames;
    let mergeDrivers = [];
    let sortDrivers = [];
    let sortedRealDrivers = [];

    realDrivers.forEach((realDriver) => {
      mergeDrivers[index] = {
        pos : realDriver.DriverPosition.Value,
        number : realDriver.Driver.RacingNumber,
        name : realDriver.Driver.BroadcastName,
        GAP : realDriver.GapToLeader,
        INT : realDriver.IntervalToPositionAhead,
        S1 : realDriver.Sectors[0].Value,
        S2 : realDriver.Sectors[1].Value,
        S3 : realDriver.Sectors[2].Value,
        LAPTIME : realDriver.LastLapTime.Value,
        PIT : realDriver.NumberOfPitStops,
        colorValue : null,
        isReal : true
      };
      index++;
      virtualDrivers.forEach((virtualDriver) => {
        if(realDriver.Driver.RacingNumber === virtualDriver.initialRacingNumber){
          mergeDrivers[index] = {
            pos : realDriver.DriverPosition.Value,
            number : realDriver.Driver.RacingNumber,
            name : virtualDriver.name,
            GAP : (parseFloat(realDriver.GapToLeader) && parseFloat(virtualDriver.gapToAdd)) ? "+".concat((Math.round((parseFloat(realDriver.GapToLeader) + parseFloat(virtualDriver.gapToAdd)) * 10) / 10)) : (realDriver.GapToLeader),
            INT : "+".concat(Math.round(virtualDriver.gapToAdd * 10) / 10),
            S1 : realDriver.Sectors[0].Value,
            S2 : realDriver.Sectors[1].Value,
            S3 : realDriver.Sectors[2].Value,
            LAPTIME : realDriver.LastLapTime.Value,
            PIT : realDriver.NumberOfPitStops,
            colorValue : virtualDriver.colorValue,
            isReal : false
          };
          index++;
        }
      });
    });
    // console.log(mergeDrivers);


    index = 0;
    sortDrivers = mergeDrivers.map((driver) => {
      if(!driver.isReal && parseFloat(driver.GAP)){
        findPos = driver.pos;
        findGAP = 0;
        mergeDrivers.forEach((tempDriver) => {
          if(tempDriver.isReal && parseFloat(tempDriver.GAP) && parseFloat(tempDriver.GAP) < parseFloat(driver.GAP) && (parseFloat(tempDriver.GAP) > findGAP || (parseFloat(tempDriver.GAP) === findGAP && parseInt(tempDriver.pos) > findPos))){
            findPos = parseInt(tempDriver.pos);
            findGAP = parseFloat(tempDriver.GAP);
          }
        });
        // console.log("Virtual Driver : " + driver.name + " position is " + findPos);
        driver.pos = findPos;
      }
      return driver;
    });
    // console.log(sortDrivers);

    mergeDrivers = [];
    tempNames  = [];
    for(index = 0, findPos = 1; index < sortDrivers.length;){
      temp = null;
      sortDrivers.forEach((driver) => {
        if(!driver.isKnown && parseInt(driver.pos) === findPos && tempNames.indexOf(driver.name) < 0){
          if(temp === null){
            temp = driver;
          }
          else{
            if(parseFloat(driver.GAP) && parseFloat(temp.GAP) && parseFloat(driver.GAP) < parseFloat(temp.GAP)) temp = driver;
          }
        }
      });
      if(temp === null){
        findPos ++;
      }
      else{
        temp.pos = index + 1;
        if(index > 0 && !temp.isReal && parseFloat(temp.GAP) && parseFloat(mergeDrivers[index-1].GAP)){
          temp.INT = "+".concat(Math.round((parseFloat(temp.GAP) - parseFloat(mergeDrivers[index-1].GAP)) * 10) / 10);
        }
        if(index > 0 && !mergeDrivers[index-1].isReal && parseFloat(temp.GAP) && parseFloat(mergeDrivers[index-1].GAP)){
          mergeDrivers[index-1].INT = mergeDrivers[index-1].INT + " | +" + Math.round((parseFloat(temp.GAP) - parseFloat(mergeDrivers[index-1].GAP)) * 10) / 10;
        }
        mergeDrivers[index] = temp;
        tempNames.push(temp.name);
        index ++;
      }
    }

    sortedRealDrivers = mergeDrivers.map((driver) => {
      return this.getTableRow(driver, index, driver.isReal);
    });

    console.log("sorted Real Drivers");
    console.log(sortedRealDrivers);
    return sortedRealDrivers;
  }
  deleteVirtualDrivers(keys){
    let nextDrivers = [];
    nextDrivers = this.state.virtualDriverValues
      .filter((driver) => {
        return !keys.find((key) => {
          return (driver.name === key);
        });
      })
      .map((driver) => driver);
    this.setState({ 
      virtualDriverValues : nextDrivers,
      tableData : this.getTableData(this.state.driverValues, nextDrivers)
    });
  }
  handleClickADD(event){
    if(!this.state.singleSelectDriverIntial){
      this.titleAndTextAlert("You must select the initial Driver.");
    }
    else if(!this.state.singleSelectColor){
      this.titleAndTextAlert("You must select the color of virtual Driver.");
    }
    else if(!this.state.gapToAdd){
      this.titleAndTextAlert("You must input the GAP to Add.");
    }
    else if(!this.state.virtualDriverName){
      this.titleAndTextAlert("You must input the name of virtual Driver.");
    }
    else {
      let virtualDriver = {
        initialRacingNumber : this.state.driverValues[this.state.singleSelectDriverIntial.value].Driver.RacingNumber,
        gapToAdd : this.state.gapToAdd,
        name : this.state.virtualDriverName,
        colorValue : this.state.singleSelectColor.value
      };
      if(this.state.virtualDriverValues.find((driver) => {return driver.name === virtualDriver.name})){
        this.titleAndTextAlert("You must input the Different name of virtual Driver.");
      }
      else{
        this.setState({
          virtualDriverValues : [...this.state.virtualDriverValues, virtualDriver],
          tableData : this.getTableData(this.state.driverValues, [...this.state.virtualDriverValues, virtualDriver])
          , checkValues : []
          , checkCount : this.state.checkCount + 1
        });
        this.successAlert("A virtual Driver has added now.");
      }
    }
  }
  handleClickDEL(){
    // console.log(this.state.checkValues);
    let key, val;
    let vals = [];
    for(key in this.state.checkValues){
      val = this.state.checkValues[key];
      if(val) vals.push(key);
    }
    if(vals.length > 0) this.warningWithConfirmMessage("Would you delete " + vals.join(", ") + " ?", vals);
    else this.titleAndTextAlert("You must select the virtual drivers before delete them.");
  }
  handleCheckbox(event){
    let name = event.target.name;
    let prevState = this.state.checkValues;
    // console.log("handle check box");
    // console.log(prevState);
    // let currentChecked = (prevState[name]) ? prevState[name] : false;
    let currentChecked = event.target.checked;
    prevState[name] = currentChecked;
    // console.log(prevState);
    this.setState({ checkValues : prevState });
  }
  render() {
    return (
      <>
        <div className="content">
          {this.state.alert}
          <Row>
            <Col md="9" sm="12">
              <Row>
                <Col xs="12" md="12">
                  <Card>
                    <CardHeader>
                      <CardTitle tag="h3">
                        <i className="tim-icons icon-notes" /> Current State of All Drivers<hr/>
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <ReactTable
                        data={this.state.tableData}
                        columns={this.state.tableColums}
                        showPagination={false}
                        defaultSorted={[{
                          id : "pos",
                          desc : false
                        }]}
                        pageSize={this.state.tableData.length}
                        getTrProps = {(state, rowInfo, column) => {
                          return {
                              style : {
                                // backgroundColor : (rowInfo.row.colorValue && (column.Header === "NAME" || column.Header === "GAP")) ? rowInfo.row.colorValue : null
                                backgroundColor : (rowInfo.row.colorValue) ? rowInfo.row.colorValue : null
                              }
                            };
                        }}
                        getTdProps = {(state, rowInfo, column) => {
                          return {
                              style : {
                                // backgroundColor : (rowInfo.row.colorValue && (column.Header === "NAME" || column.Header === "GAP")) ? rowInfo.row.colorValue : null
                                visibility : (rowInfo.row.colorValue && (column.Header === "NO" || column.Header === "S1" || column.Header === "S2" || column.Header === "S3" || column.Header === "TIME" || column.Header === "PIT")) ? "hidden" : null
                              }
                            };
                        }}
                      />
                    </CardBody>
                    <CardFooter>
                    </CardFooter>
                  </Card>
                </Col>
              </Row>
              <Row>
                <Col md="5" sm="12">
                  <Card>
                    <CardHeader>
                      <CardTitle tag="h3">
                        <i className="tim-icons icon-compass-05" /> Specifications <hr/>
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <Form className="form-horizontal">
                        <Row>
                          <Label md="5">Initial Driver</Label>
                          <Col md="7">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select primary"
                                classNamePrefix="react-select"
                                name="singleSelectInitial"
                                value={this.state.singleSelectDriverIntial}
                                onChange={value => this.setState({ singleSelectDriverIntial : value})}
                                options={this.state.singleSelectDriverValues}
                                placeholder="Initial Driver"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="5">GAP to Add</Label>
                          <Col md="7">
                            <FormGroup className="has-success">
                              <Input 
                                type="text"
                                name="textGAPtoAdd"
                                id="text_GAP_Add"
                                placeholder="Input the GAP to add"
                                value={this.state.gapToAdd}
                                onChange={value => this.setState({gapToAdd : value.target.value})}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="5" sm="12">
                  <Card>
                    <CardHeader>
                      <CardTitle tag="h3">
                        <i className="tim-icons icon-palette" /> Design <hr/>
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <Form className="form-horizontal">
                        <Row>
                          <Label md="5">Name</Label>
                          <Col md="7">
                            <FormGroup className="has-success">
                              <Input 
                                type="text"
                                name="textGAPtoAdd"
                                id="text_GAP_Add"
                                placeholder="Input the virtual driver name"
                                value={this.state.virtualDriverName}
                                onChange={value => this.setState({virtualDriverName : value.target.value})}
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                        <Row>
                          <Label md="5">Color</Label>
                          <Col md="7">
                            <FormGroup>
                              <Select
                                md="9"
                                className="react-select success"
                                classNamePrefix="react-select"
                                name="singleSelectColor"
                                value={this.state.singleSelectColor}
                                onChange={value => this.setState({ singleSelectColor : value})}
                                options={this.state.singleSelectColorValues}
                                placeholder="Select Color"
                              />
                            </FormGroup>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="2" sm="12">
                  <Card>
                    <CardHeader>
                      <CardTitle tag="h3">
                        <i className="tim-icons icon-tap-02" /> Action <hr/>
                      </CardTitle>
                    </CardHeader>
                    <CardBody>
                      <Form className="form-horizontal">
                        <Button 
                          color="info" 
                          className="animation-on-hover btn-round" 
                          style={{width : '100%'}}
                          onClick={this.handleClickADD}
                        >
                          <i className="tim-icons icon-simple-add" /> ADD
                        </Button>
                        <Button 
                          color="danger" 
                          className="animation-on-hover" 
                          style={{width : '100%'}}
                          onClick={this.handleClickDEL}
                        >
                          <i className="tim-icons icon-trash-simple" /> DELETE
                        </Button>
                      </Form>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
            <Col md="3" sm="12">
              <RightSidebar {...this.props} />
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Ghost;
