import React from "react";
import ConnectAPI from "../../connectAPI";
// react plugin used to create DropdownMenu for selecting items
import Select from "react-select";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  FormGroup,
  Container,
  Row,
  Col,
  Alert 
} from "reactstrap";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      singleSelectChampion: null,
      singleSelectTeam: null,
      singleSelectChampionValues: [],
      singleSelectTeamValues: [],
      teamValues: []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeChampionship = this.onChangeChampionship.bind(this);
  }
  componentDidMount() {
    document.body.classList.toggle("login-page");
    var conn = new ConnectAPI();
    this.setState({
      errorMessage : "",
      singleSelectChampionValues : [
      {
        value: "",
        label: "Championship",
        isDisabled: true
      },
      { value: "f2", label: "Formula 2" },
      { value: "f3", label: "Formula 3" }],
      teamValues : [
        {
          value: "",
          label: "Team",
          isDisabled: true
        }]
    });
    conn.getFetch('team', '')
      .then(data => {
        data = JSON.parse(data);
        if(data.length > 0){
          for(let i = 0; i < data.length; i++){
            this.setState({
              teamValues : [...this.state.teamValues, 
                {
                  value: data[i].id,
                  label: data[i].team_name,
                  championship: data[i].championship,
                  team_logo: data[i].team_logo
                }]
            });
          }
          this.setState({ singleSelectTeamValues : this.state.teamValues });
        }
      });
  }
  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }
  handleSubmit(event){
    event.preventDefault();
    const data = new FormData(event.target);
    let username = data.get('username');
    let password = data.get('password');
    if(username === ''){
      this.setState({errorMessage : "You must input User ID."});
    }
    else if(password === ''){
      this.setState({errorMessage : "You must input Password."});
    }
    else if(this.state.singleSelectChampion == null){
      this.setState({errorMessage : "You must select Championship."});
    }
    else if(this.state.singleSelectTeam == null){
      this.setState({errorMessage : "You must select Team."});
    }
    else{
      var conn = new ConnectAPI();
      conn.postFetch('user', {username: username, password: password})
        .then(data => {
          if(data.length > 0 && typeof data[0].id != undefined){
            localStorage.setItem('userId', data[0].id);
            localStorage.setItem('teamId', this.state.singleSelectTeam.value);
            localStorage.setItem('team_name', this.state.singleSelectTeam.label);
            // localStorage.setItem('championship', this.state.singleSelectTeam.championship);
            localStorage.setItem('team_logo', this.state.singleSelectTeam.team_logo);
            localStorage.setItem('championship', this.state.singleSelectChampion.value);
            window.location.href = "/admin/dashboard";
          }
          else{
            localStorage.setItem('userId', 0);
            this.setState({errorMessage : "Your User ID and Password is incorrect."});
          }
        });
    }
  }
  onChangeChampionship(value){
      this.setState({ singleSelectChampion: value });
      let tempValues = this.state.teamValues;
      let teamResult = [tempValues[0]];
      for(let i = 1; i < tempValues.length; i++){
        if(tempValues[i].championship === value.value){
          teamResult.push(tempValues[i]);
        }
        else{
        }
      }
      this.setState({ singleSelectTeamValues: teamResult });
  }
  render() {
    return (
      <>
        <div className="content">
          <Container>
            <Col className="ml-auto mr-auto" lg="8" md="10" sm="12">
              <Form className="form" onSubmit={this.handleSubmit}>
                <Card className="card-login card-white">
                  <CardHeader>
                    <img
                      alt="..."
                      src={require("assets/img/card-primary.png")}
                    />
                    <CardTitle tag="h1">Log in</CardTitle>
                  </CardHeader>
                  <CardBody>
                  <Alert 
                    color="danger"
                    isOpen={this.state.errorMessage !== ""}
                  >
                    {this.state.errorMessage}
                  </Alert>
                    <Row>
                      <Col className="" lg="6" md="6" sm="12">
                        <CardTitle tag="h4">USER ID</CardTitle>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="tim-icons icon-badge" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input placeholder="Type your User ID" name="username" type="text" />
                        </InputGroup>
                        <CardTitle tag="h4">PASSWORD</CardTitle>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="tim-icons icon-lock-circle" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input placeholder="Type your Password" name="password" type="password" />
                        </InputGroup>
                      </Col>
                      <Col className="" lg="6" md="6" sm="12">
                        <CardTitle tag="h4">CHAMPIONSHIP</CardTitle>
                        <FormGroup>
                          <Select
                            className="react-select info"
                            classNamePrefix="react-select"
                            name="singleSelect"
                            value={this.state.singleSelectChampion}
                            onChange={this.onChangeChampionship}
                            options={this.state.singleSelectChampionValues}
                            placeholder="Select Championship"
                          />
                        </FormGroup>
                        <CardTitle tag="h4">TEAM</CardTitle>
                        <FormGroup>
                          <Select
                            className="react-select info"
                            classNamePrefix="react-select"
                            name="singleSelect"
                            value={this.state.singleSelectTeam}
                            onChange={value =>
                              this.setState({ singleSelectTeam: value })
                            }
                            options={this.state.singleSelectTeamValues}
                            placeholder="Select Team"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </CardBody>
                  <CardFooter>
                    <Row
                      className="ml-auto mr-auto mb-3"
                    >
                      <Button
                        className=""
                        color="warning"
                        size="lg"
                      >
                        FORGOT YOUR ID OR PASSWORD?
                      </Button>
                      <Button
                        className="ml-auto mr-auto"
                        color="primary"
                        size="lg"
                        type="submit"
                      >
                        LOG IN
                      </Button>
                    </Row>
                    <Row>
                      <Col className="">
                        <h6>
                          <a
                            className="link footer-link"
                            href="/auth/register"
                          >
                            Create Account
                          </a>
                        </h6>
                      </Col>
                    </Row>
                  </CardFooter>
                </Card>
              </Form>
            </Col>
          </Container>
        </div>
      </>
    );
  }
}

export default Login;
