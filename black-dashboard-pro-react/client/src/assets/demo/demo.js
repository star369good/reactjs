type = ['primary', 'info', 'success', 'warning', 'danger'];
myChart = null;
myChartTwo = null;
chartCheckOption = null;
labels = ['','','','','','','','','','','','','','','','','','','',''];
datas = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
labelsTwo = ['', '', ''];
datasTwo = [0, 0, 0];
feedSelected = 'Feed';

demo = {

  initPickColor: function() {
    $('.pick-class-label').click(function() {
      var new_class = $(this).attr('new-class');
      var old_class = $('#display-buttons').attr('data-class');
      var display_div = $('#display-buttons');
      if (display_div.length) {
        var display_buttons = display_div.find('.btn');
        display_buttons.removeClass(old_class);
        display_buttons.addClass(new_class);
        display_div.attr('data-class', new_class);
      }
    });
  },

  initDocChart: function() {
    chartColor = "#FFFFFF";

    // General configuration for the charts with Line gradientStroke
    gradientChartOptionsConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        bodySpacing: 4,
        mode: "nearest",
        intersect: 0,
        position: "nearest",
        xPadding: 10,
        yPadding: 10,
        caretPadding: 10
      },
      responsive: true,
      scales: {
        yAxes: [{
          display: 0,
          gridLines: 0,
          ticks: {
            display: false
          },
          gridLines: {
            zeroLineColor: "transparent",
            drawTicks: false,
            display: false,
            drawBorder: false
          }
        }],
        xAxes: [{
          display: 0,
          gridLines: 0,
          ticks: {
            display: false
          },
          gridLines: {
            zeroLineColor: "transparent",
            drawTicks: false,
            display: false,
            drawBorder: false
          }
        }]
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 15,
          bottom: 15
        }
      }
    };

    ctx = document.getElementById('lineChartExample').getContext("2d");

    gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#80b6f4');
    gradientStroke.addColorStop(1, chartColor);

    gradientFill = ctx.createLinearGradient(0, 170, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(249, 99, 59, 0.40)");

    myChart = new Chart(ctx, {
      type: 'line',
      responsive: true,
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Active Users",
          borderColor: "#f96332",
          pointBorderColor: "#FFF",
          pointBackgroundColor: "#f96332",
          pointBorderWidth: 2,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 1,
          pointRadius: 4,
          fill: true,
          backgroundColor: gradientFill,
          borderWidth: 2,
          data: [542, 480, 430, 550, 530, 453, 380, 434, 568, 610, 700, 630]
        }]
      },
      options: gradientChartOptionsConfiguration
    });
  },

  initDashboardPageCharts: function(){
    // General configuration for the charts with Line gradientStroke
    gradientChartOptionsConfiguration =  {
      maintainAspectRatio: false,
      legend: {
            display: false
      },

      tooltips: {
        backgroundColor: '#fff',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales:{
        yAxes: [{
          barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                  color: 'rgba(29,140,248,0.0)',
                  zeroLineColor: "transparent",
              },
              ticks: {
                suggestedMin:0,
                suggestedMax: 30,
                  padding: 20,
                  fontColor: "#9a9a9a"
              }
            }],

        xAxes: [{
          barPercentage: 1.6,
              gridLines: {
                drawBorder: false,
                  color: 'rgba(220,53,69,0.1)',
                  zeroLineColor: "transparent",
              },
              ticks: {
                  padding: 20,
                  fontColor: "#9a9a9a"
              }
            }]
        }
    };

    var ctx = document.getElementById("lineChartExample").getContext("2d");

    var gradientStroke = ctx.createLinearGradient(0,230,0,50);

    gradientStroke.addColorStop(1, 'rgba(72,72,176,0.2)');
    gradientStroke.addColorStop(0.2, 'rgba(72,72,176,0.0)');
    gradientStroke.addColorStop(0, 'rgba(119,52,169,0)'); //purple colors

    var data = {
      labels: labels,
      datasets: [{
        label: "Data",
        fill: true,
        backgroundColor: gradientStroke,
        borderColor: '#d048b6',
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: '#d048b6',
        pointBorderColor:'rgba(255,255,255,0)',
        pointHoverBackgroundColor: '#d048b6',
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 4,
        data: datas,
      }]
    };

    myChart = new Chart(ctx, {
      type: 'line',
      data: data,
      options: gradientChartOptionsConfiguration
    });


    // ChartTwo
    gradientBarChartConfiguration = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{

          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 120,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{

          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };

    var ctx1 = document.getElementById("ChartTwo").getContext("2d");

    var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(29,140,248,0.2)');
    gradientStroke1.addColorStop(0.4, 'rgba(29,140,248,0.0)');
    gradientStroke1.addColorStop(0, 'rgba(29,140,248,0)'); //blue colors


    myChartTwo = new Chart(ctx1, {
      type: 'bar',
      responsive: true,
      legend: {
        display: false
      },
      data: {
        labels: labelsTwo,
        datasets: [{
          label: feedSelected,
          fill: true,
          backgroundColor: gradientStroke1,
          hoverBackgroundColor: gradientStroke1,
          borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          data: datasTwo,
        }]
      },
      options: gradientBarChartConfiguration
    });

  },

  showNotification: function(from, align) {
    color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "tim-icons icon-bell-55",
      message: "Welcome to <b>Black Dashboard</b> - a beautiful freebie for every web developer."

    }, {
      type: type[color],
      timer: 8000,
      placement: {
        from: from,
        align: align
      }
    });
  },

  updateChart: function(lbls, dts, flagSet=true){
    if(flagSet){
      labels = lbls;
      datas = dts;
      console.log(datas);
    }
    
    myChart.data.labels = labels;
    if(chartCheckOption == 'bestlaptime') myChart.data.datasets[0].data = datas.bestLapTime;
    else if(chartCheckOption == 'lastlaptime') myChart.data.datasets[0].data = datas.lastLapTime;
    else myChart.data.datasets[0].data = datas.position;
    myChart.update();

    myChartTwo.data.labels = labelsTwo;
    myChartTwo.data.datasets[0].label = feedSelected;

    var pos1 = 0, pos2 = 0, pos3 = 0, i;
    for(i = 0; i < labels.length; i++){
      if(labelsTwo[0].indexOf(labels[i]) > -1) pos1 = i;
      if(labelsTwo[1].indexOf(labels[i]) > -1) pos2 = i;
      if(labelsTwo[2].indexOf(labels[i]) > -1) pos3 = i;
    }

    if(feedSelected == 'Best Lap Time'){
      datasTwo = [datas.bestLapTime[pos1], datas.bestLapTime[pos2], datas.bestLapTime[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.bestLapTimeMax;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.bestLapTimeMin;
    }
    else if(feedSelected == 'GAP'){
      datasTwo = [datas.gap[pos1], datas.gap[pos2], datas.gap[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.gapMax;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.gapMin;
    }
    else if(feedSelected == 'Interval'){
      datasTwo = [datas.interval[pos1], datas.interval[pos2], datas.interval[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.intervalMax;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.intervalMin;
    }
    else if(feedSelected == 'Last Lap Time'){
      datasTwo = [datas.lastLapTime[pos1], datas.lastLapTime[pos2], datas.lastLapTime[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.lastLapTimeMax;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.lastLapTimeMin;
    }
    else if(feedSelected == 'Sector 1'){
      datasTwo = [datas.sector1[pos1], datas.sector1[pos2], datas.sector1[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.sector1Max;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.sector1Min;
    }
    else if(feedSelected == 'Sector 2'){
      datasTwo = [datas.sector2[pos1], datas.sector2[pos2], datas.sector2[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.sector2Max;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.sector2Min;
    }
    else if(feedSelected == 'Sector 3'){
      datasTwo = [datas.sector3[pos1], datas.sector3[pos2], datas.sector3[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.sector3Max;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.sector3Min;
    }
    else if(feedSelected == 'PIT'){
      datasTwo = [datas.pit[pos1], datas.pit[pos2], datas.pit[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.pitMax;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.pitMin;
    }
    else{//Position
      datasTwo = [datas.position[pos1], datas.position[pos2], datas.position[pos3]];
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMax = datas.positionMax;
      myChartTwo.options.scales.yAxes[0].ticks.suggestedMin = datas.positionMin;
    }

    myChartTwo.data.datasets[0].data = datasTwo;
    myChartTwo.update();
  }

};

$("#check-position").click(function() {
  chartCheckOption = 'position';
  myChart.options.scales.yAxes[0].ticks.suggestedMax = datas.positionMax;
  myChart.options.scales.yAxes[0].ticks.suggestedMin = datas.positionMin;
  demo.updateChart(null, null, false);
});

$("#check-bestlaptime").click(function() {
  chartCheckOption = 'bestlaptime';
  myChart.options.scales.yAxes[0].ticks.suggestedMax = datas.bestLapTimeMax;
  myChart.options.scales.yAxes[0].ticks.suggestedMin = datas.bestLapTimeMin;
  demo.updateChart(null, null, false);
});

$("#check-lastlaptime").click(function() {
  chartCheckOption = 'lastlaptime';
  demo.updateChart(null, null, false);
});

$('#first-driver').change(function(){
  labelsTwo[0] = $(this).val().trim();
  demo.updateChart(null, null, false);
});

$('#second-driver').change(function(){
  labelsTwo[1] = $(this).val().trim();
  demo.updateChart(null, null, false);
});

$('#third-driver').change(function(){
  labelsTwo[2] = $(this).val().trim();
  demo.updateChart(null, null, false);
});

$('#select-feed').change(function(){
  feedSelected = $(this).val().trim();
  demo.updateChart(null, null, false);
});