import axios from 'axios';

class ConnectAPI{
    defaultUrl = 'http://localhost:9000/test/';

    getFetch = (url, query) => {
        return fetch(`${this.defaultUrl}${url}?${query}`, 
            {
                headers : { 
                'Content-Type': 'application/json',
                'Accept': 'application/json'
                }
            })
            .then(result => result.text())
            .then(data => data)
            .catch(err => {
                console.error(err);
            });
    };

    postFetch = (url, data) => {
        return axios.post(`${this.defaultUrl}${url}`, 
            data, 
            {
                headers: { 
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                }
            })
            .then(result => result.data)
            .then(data => data)
            .catch(err => {
                console.error(err);
            });
    };
}

export default ConnectAPI;