import React from "react";
import { Redirect  } from "react-router-dom";

export default function(props, ComposedComponent){
    class Authenticate extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                flagAuthenticated : false
            };
        }
        componentWillMount(){
            if(localStorage.getItem('userId') > 0){
                this.setState({flagAuthenticated : true});
            } 
        }
        render(){
            if(this.state.flagAuthenticated) return <ComposedComponent {...props} />
            else return <Redirect  to="/auth/login" />
        }
    }
    return <Authenticate {...props}/>;
};